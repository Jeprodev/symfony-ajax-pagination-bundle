<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Twig\Extension
 * @link            http://jeprodev.net
 *
 * @copyright (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Twig\Extension;

use Jeprodev\Paginator\Pagination\PaginationInterface;
use Jeprodev\Paginator\Twig\Processor\PaginatorProcessor;
use Twig\Extension\AbstractExtension;
use Twig\Environment;
use Twig\TwigFunction;

class PaginationExtension extends AbstractExtension
{
    /**
     * @var PaginatorProcessor $processor
     */
    private $processor;

    public function __construct(PaginatorProcessor $processor)
    {
        $this->processor = $processor;
    }

    public function getFunctions(): array
    {
        $options = ['is_safe' => ['html'], "needs_environment" => true];

        return [
            new TwigFunction('renderPagination', [$this, 'renderPagination'], $options),
            new TwigFunction('renderPaginationSortable', [$this, 'renderSortablePagination'], $options),
            new TwigFunction('renderPaginationFilter', [$this, 'renderFilterPagination'], $options)
        ];
    }

    /**
     * Renders the pagination template.
     *
     * @param   Environment         $environment
     * @param   PaginationInterface $pagination
     * @param   string|null         $template
     * @param   array|null          $queryParams
     * @param   array|null          $viewParams
     * @return  string
     */
    public function renderPagination(Environment $environment, PaginationInterface $pagination, ?array $queryParams = [], ?array $viewParams = [], ?string $template = null): string
    {
        return $environment->render(
            $template ?: $pagination->getTemplate(),
            $this->processor->processPaginationParams($pagination, $queryParams ?? [], $viewParams ?? [])
        );
    }

    /**
     * Create a sort url for the field named $title and identified by $key which consists of
     * alias and field. $options holds all link parameters like "alt, class" and so on.
     *
     * @param   Environment             $environment
     * @param   PaginationInterface     $pagination
     * @param   string                  $title
     * @param   [type]                  $key
     * @param   array                   $options
     * @param   array                   $params
     * @param   string|null             $template
     * @return  string
     */
    public function renderSortablePagination(Environment $environment, PaginationInterface $pagination, string $title, $key, array $options = [], array $params = [], ?string $template = null): string
    {
        return $environment->render(
            $template ?: $pagination->getSortableTemplate(),
            $this->processor->renderSortablePagination($pagination, $title, $options, $params)
        );
    }

    /**
     * Create a filter url for the field named $title and identified by $key which consists of
     * alias and field. $options holds all link parameters like "alt, class" and so on.
     *
     * @param   Environment         $environment
     * @param   PaginationInterface $pagination
     * @param   array               $fields
     * @param   array|null          $options
     * @param   array|null          $params
     * @param   string|null         $template
     * @return  string
     */
    public function renderFilterPagination(Environment $environment, PaginationInterface $pagination, array $fields, ?array $options = [], ?array $params = [], ?string $template = null): string
    {
        return $environment->render(
            $template ?: $pagination->getFilterTemplate(),
            $this->processor->renderFilterPagination($pagination, $fields, $options ?? [], $params ?? [])
        );
    }
}
