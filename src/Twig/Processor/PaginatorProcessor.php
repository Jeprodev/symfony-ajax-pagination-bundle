<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Twig\Extension
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Twig\Processor;

use Jeprodev\Paginator\Pagination\PaginationInterface;
use Jeprodev\Paginator\Paginator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PaginatorProcessor
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var TranslatorInterface $translator
     */
    private $translator;

    public function __construct(UrlGeneratorInterface $router, TranslatorInterface $translator)
    {
        $this->router = $router;
        $this->translator = $translator;
    }

    public function getRouter(): UrlGeneratorInterface
    {
        return $this->router;
    }

    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * Genertes template data
     *
     * @param PaginationInterface $pagination
     * @param array $queryParams
     * @param array $viewParams
     * @return array
     */
    public function processPaginationParams(PaginationInterface $pagination, array $queryParams = [], array $viewParams = []): array
    {
        $data = $pagination->getPaginationData();

        $data['route'] = $pagination->getPageRoute();
        $data['query'] = array_merge($pagination->getQueryPaginationParameters(), $queryParams);

        $options = $pagination->getPaginatorOptions();
        $data['page_parameter_name'] = $options[Paginator::PAGE_PARAMETER_NAME];

        return array_merge(
            $pagination->getPaginatorOptions(),
            $pagination->getQueryPaginationParameters(),
            $viewParams,
            $data
        );
    }

    public function renderFilterPagination(PaginationInterface $pagination, array $fields, array $options = [], array $params = []): array
    {
        $options = array_merge(
            [
                'absolute' => UrlGeneratorInterface::ABSOLUTE_PATH,
                'translation' => ['parameters' => [], 'domain' => null],
                'button' => 'Filter'
            ],
            $options
        );

        $params = array_merge($pagination->getQueryPaginationParameters(), $params);
        $params[$pagination->getPaginatorOption('page.name')] = 1;

        $filterFieldName = $pagination->getPaginatorOption('filter.field');
        $filterValueName = $pagination->getPaginatorOption('filter.value');

        $selectedField = $params[$filterFieldName] ?? null;
        $selectedValue = $params[$filterValueName] ?? null;

        $action = $this->router->generate($pagination->getPageRoute(), $params, $options['absolute']);

        foreach ($fields as $field => $title) {
            $fields[$field] = $this->translator->trans($title, $options['translation']['parameters'], $options['translation']['domain']);
        }

        $options['button'] = $this->translator->trans($options['button'], $options['translation']['parameters'], $options['translation']['domain']);

        unset($options['absolute'], $options['translation']);

        return array_merge(
            $pagination->getPaginatorOptions() ?? [],
            $pagination->getQueryPaginationParameters() ?? [],
            compact('fields', 'action', 'filterFieldName', 'filterValueName', 'selectedField', 'selectedValue', 'options')
        );
    }

    /**
     * @param PaginationInterface $pagination
     * @param string $title
     * @param [type] $key
     * @param array $options
     * @param array $params
     * @return array
     */
    public function renderSortablePagination(PaginationInterface $pagination, string $title, $key, array $options = [], array $params = []): array
    {
        $options = array_merge(
            [
                'absolute' => UrlGeneratorInterface::ABSOLUTE_PATH,
                'translation' => ['parameters' => [], 'domain' => null, 'count' => null],
                'button' => 'Filter'
            ],
            $options
        );

        $hasFixedDirection = null !== $pagination->getPaginatorOption('sort.direction') && isset($params[$pagination->getPaginatorOption('sort.direction')]);

        $params = array_merge($pagination->getQueryPaginationParameters(), $params);

        $direction = $options['sort']['direction'] ?? 'asc';

        if (null !== $pagination->getPaginatorOption('sort.direction')) {
            if (isset($params[$pagination->getPaginatorOption('sort.direction')])) {
                $direction = $params[$pagination->getPaginatorOption('sort.direction')];
            } elseif (isset($options[$pagination->getPaginatorOption('sort.direction')])) {
                $direction = $options[$pagination->getPaginatorOption('sort.direction')];
            }
        }

        $sorted = $pagination->isSorted($key, $params);

        if ($sorted) {
            if (!$hasFixedDirection) {
                $direction = 'asc' === strtolower($direction) ? 'desc' : 'asc';
            }

            $class = 'asc' === $direction ? 'desc' : 'asc';
        } else {
            $class = 'sortable';
        }

        $options['class'] = (isset($options['class']) ? $options['class'] . ' ' : '') . $class;

        if (is_array($title) && array_key_exists($direction, $title)) {
            $title = $title[$direction];
        }

        if (is_array($key)) {
            $key = implode('+', $key);
        }

        $params = array_merge(
            $params,
            [
                $pagination->getPaginatorOption('sort.field') => $key,
                $pagination->getPaginatorOption('sort.direction') => $direction,
                $pagination->getPaginatorOption('pagae.name') => 1
            ]
        );

        $options['href'] = $this->router->generate($pagination->getPageRoute(), $params, $options['absolute']);

        if (null !== $options['translation']['domain']) {
            if (null == $options['translation']['count']) {
                $parameters = $options['translation']['parameters'];
            } else {
                $parameters = $options['translation']['parameters'] + ['%count%' => $options['translation']['count']];
            }

            $title = $this->translator->trans($title, $parameters, $options['translation']['domain']);
        }

        if (!isset($options['title'])) {
            $options['title'] = $title;
        }

        unset($options['absolute'], $options['translation']['parameters'], $options['translation']['domain'], $options['translation']['count']);

        return array_merge(
            $pagination->getPaginatorOptions() ?? [],
            $pagination->getQueryPaginationParameters() ?? [],
            compact('options', 'title', 'direction', 'sorted', 'key')
        );
    }
}
