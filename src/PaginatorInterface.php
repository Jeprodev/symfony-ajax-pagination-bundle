<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator;

use Jeprodev\Paginator\Pagination\PaginationInterface;

interface PaginatorInterface
{
    public const DEFAULT_SORT_FIELD_NAME = 'ajax_paginator.default.sort.field_name';

    public const DEFAULT_SORT_DIRECTION = 'ajax_paginator.default.sort.direction';

    public const DEFAULT_PAGE_RANGE = 'ajax_paginator.pagination.range';

    public const DEFAULT_PAGE_ITEMS_LIMIT = 'ajax_paginator.pagination.limit';

    public const DEFAULT_PAGE_ITEMS_LIMIT_VALUE = 20;

    public const PAGINATOR_PAGINATION_TEMPLATE = 'ajax_paginator.template.pagination';

    public const PAGINATOR_FILTRATION_TEMPLATE = 'ajax_paginator.template.filtration';

    public const PAGINATOR_SORTABLE_TEMPLATE = 'ajax_paginator.template.sortable';

    public const REMOVE_DEFAULT_SORT_PARAMS = 'ajax_paginator_remove_default_sort_params';

    public const FILTER_FIELD_PARAMETER_NAME = 'ajax_paginator.filter.field';

    public const FILTER_VALUE_PARAMETER_NAME = 'ajax_paginator.filter.value';

    public const SORT_DIRECTION_PARAMETER_NAME = 'ajax_paginator.sort.direction';

    public const SORT_FIELD_PARAMETER_NAME = 'ajax_paginator.sort.field';

    public const SORT_FIELD_PARAMETER_VALUE = 'ajax_paginator.sort.field_value';

    public const SORT_FIELD_ALLOWED_FIELDS = 'ajax_paginator.sort.allowed_fields';

    public const PAGE_PARAMETER_NAME = 'ajax_paginator.pagination.name';

    public const DISTINCT = 'ajax_paginator.distinct';

    public const USE_AJAX = 'ajax_paginator.pagination.use_ajax';

    public const USE_AJAX_PARAMETERS = 'ajax_paginator.pagination.ajax_parameters';

    public const USE_AJAX_ROUTE_PARAMETER = 'ajax_paginator.pagination.ajax_retrieve_route';

    public const PAGE_OUT_OF_RANGE_BEHAVIOR = 'ajax_paginator.pagination.out_of_range';

    public const PAGE_OUT_OF_RANGE_IGNORE = 'ignore';

    public const PAGE_OUT_OF_RANGE_FIX = 'fix';

    public const PAGE_OUT_OF_RANGE_THROW_EXCEPTION = 'throwException';

    public function setDefaultPaginatorOptions(array $options);

    /**
     * Paginates anything that needs to be paginated
     *
     * @param   mixed                   $collection     A collection of object to paginate
     * @param   integer                 $page           The page number, starting from 1
     * @param   integer                 $limit          The number of items per page
     * @param   array<string, mixed>    $options
     * @return  PaginationInterface
     *
     * @throws  LogicException
     */
    public function paginate($collection, int $page = 1, int $limit = PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT_VALUE, array $options = []): ?PaginationInterface;
}
