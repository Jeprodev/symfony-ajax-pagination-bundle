<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Pagination;

use ArrayObject;
use Iterator;
use Jeprodev\Paginator\Paginator;

abstract class AbstractPagination implements Iterator, PaginationInterface
{
    private ?string $route = null;

    /**
     * @var int $page_range The pagination page range
     */
    private int $page_range = 5;

    /**
     * @var int $current_page
     */
    private int $current_page = 1;

    private iterable $page_items = [];

    /**
     * @var int $number_of_items_per_page
     */
    private int $number_of_items_per_page = 20;

    /**
     * A target being paginated
     *
     * @var mixed
     */
    private $collection = null;

    private $collection_total = 0;

    /**
     * The parameters stored in the request query to customize paginator.
     *
     * @var array|null $query_pagination_parameters
     */
    protected array $query_pagination_parameters = [];

    protected ?array $paginator_options = null;

    protected function __construct(array $params)
    {
        $this->paginator_options = [];
        $this->query_pagination_parameters = $params;
    }

    public function getPageRange(): int
    {
        return abs($this->page_range);
    }

    public function setPageRange(int $range): PaginationInterface
    {
        $this->page_range = abs($range);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentPage(): int
    {
        /*$page = 1;

        if (isset($this->query_pagination_parameters[$this->getPaginatorOption(Paginator::PAGE_PARAMETER_NAME)])) {
            $page = $this->query_pagination_parameters[$this->getPaginatorOption(Paginator::PAGE_PARAMETER_NAME)];
        }

        return  $page != null ? $page : 1;*/
        return $this->current_page;
    }

    /**
     * {@inheritDoc}
     */
    public function setCurrentPage(int $page): PaginationInterface
    {
        //$this->query_pagination_parameters[$this->getPaginatorOption(Paginator::PAGE_PARAMETER_NAME)] = $page;
        $this->current_page = $page;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getItemsPerPage(): int
    {
        /*$limit = $this->paginator_options[Paginator::DEFAULT_PAGE_ITEMS_LIMIT];

        if ($limit === null) {
            $limit = Paginator::DEFAULT_PAGE_ITEMS_LIMIT_VALUE;
        }

        return $limit;*/
        return $this->number_of_items_per_page;
    }

    /**
     * {@inheritDoc}
     */
    public function setItemsPerPage(?int $limit): PaginationInterface
    {
        //$this->paginator_options[Paginator::DEFAULT_PAGE_ITEMS_LIMIT] = $limit;
        $this->number_of_items_per_page = ($limit != null) ? $limit : 20;
        return $this;
    }

    public function getPageItems(): iterable
    {
        return $this->page_items;
    }

    public function setPageItems(?iterable $items): PaginationInterface
    {
        if ($items === null) {
            $items = [];
        }
        $this->page_items = $items;
        return $this;
    }
/*
    public function setPagesLimit(?int $limit): PaginationInterface
    {
        $this->paginator_options[Paginator::DEFAULT_PAGE_ITEMS_LIMIT] = $limit;
        return $this;
    }
*/
    public function getPageCount(): int
    {
        $count = 1;
        if (is_countable($this->collection) || is_array($this->collection)) {
            $count = (int)ceil(count($this->collection) / $this->getItemsPerPage());
        } elseif ($this->getCollectionTotal() > 0) {
            $count = (int)ceil($this->getCollectionTotal() / $this->getItemsPerPage());
        }

        $pagesLimit = $this->paginator_options[Paginator::DEFAULT_PAGE_ITEMS_LIMIT] ?? null;

        if (null !== $pagesLimit) {
            $count = min($count, $pagesLimit);
        }

        return max($count, 1);
    }

    /**
     * {@inheritDoc}
     */
    public function getQueryPaginationParameters(): array
    {
        return $this->query_pagination_parameters;
    }

    /**
     * {@inheritDoc}
     */
    public function setQueryPaginationParameters(array $params = []): PaginationInterface
    {
        $this->query_pagination_parameters = array_merge($this->query_pagination_parameters, $params);
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getQueryPaginationParameter(string $name)
    {
        return $this->query_pagination_parameters[$name] ?? null;
    }

    public function setQueryPaginationParameter(string $key, $value): PaginationInterface
    {
        $this->query_pagination_parameters[$key] = $value;
        return $this;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function setCollection($items): PaginationInterface
    {
        if ($items == null) {
            $this->collection = [];
        } else {
            $this->collection = $items;
        }
        return $this;
    }

    public function getCollectionTotal(): int
    {
        return $this->collection_total;
    }

    /**
     * {@inheritDoc}
     */
    public function setCollectionTotal(?int $total = 0): PaginationInterface
    {
        $this->collection_total = $total;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getPaginatorOption(string $name)
    {
        return $this->paginator_options[$name] ?? null;
    }

    /**
     * {@inheritDoc}
     */
    public function getPaginatorOptions(): array
    {
        return $this->paginator_options;
    }

    /**
     * {@inheritDoc}
     */
    public function setPaginatorOptions(array $options = []): PaginationInterface
    {
        $this->paginator_options = array_merge($this->paginator_options, $options);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getPageRoute(): ?string
    {
        if ($this->getPaginatorOption(Paginator::USE_AJAX)) {
            return $this->getPaginatorOption(Paginator::USE_AJAX_ROUTE_PARAMETER);
        }
        return $this->route;
    }

    /**
     * {@inheritDoc}
     */
    public function setPageRoute(?string $route): PaginationInterface
    {
        $this->route = $route;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function rewind(): void
    {
        if (is_object($this->page_items)) {
            $items = get_mangled_object_vars($this->page_items);
            reset($items);
            $this->page_items = new ArrayObject($items);
        } else {
            reset($this->page_items);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function current(): mixed
    {
        return current($this->page_items);
    }

    /**
     * @return bool|float|int|string|null
     */
    public function key(): mixed
    {
        if (is_object($this->page_items)) {
            $items = get_mangled_object_vars($this->page_items);

            return key($items);
        }

        return key($this->page_items);
    }

    /**
     * {@inheritDoc}
     */
    public function next(): void
    {
        next($this->page_items);
    }

    /**
     * {@inheritDoc}
     */
    public function valid(): bool
    {
        return key($this->page_items) !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function count(): int
    {
        return count($this->page_items);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($offset): mixed
    {
        return $this->page_items[$offset];
    }

    /**
     * @param string|int|float|bool|null $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value): void
    {
        if (null === $offset) {
            $this->page_items[] = $value;
        } else {
            $this->page_items[$offset] = $value;
        }
    }

    /**
     * @param   mixed $offset
     * @return  void
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->page_items[$offset]);
    }

    /**
     * @param string|int|float|bool|null $offset
     */
    public function offsetExists($offset): bool
    {
        if ($this->page_items instanceof \ArrayIterator) {
            return array_key_exists($offset, iterator_to_array($this->page_items));
        }

        $array = [];
        array_push($array, $this->page_items);
        return array_key_exists($offset, $array);
    }
}
