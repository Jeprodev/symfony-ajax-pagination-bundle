<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Pagination;

use ArrayAccess;
use Countable;
use Traversable;

/**
 * Pagination interface strictly defines the methods a paginator will use
 * to populate the pagination data.
 */
interface PaginationInterface //extends Countable, Traversable, ArrayAccess
{
    public function getPageRoute(): ?string;

    public function setPageRoute(?string $route): PaginationInterface;

    public function getSortField(): ?string;

    /**
     * Get currently used page number
     *
     * @return integer
     */
    public function getCurrentPage(): int;

    /**
     * Set the current page
     *
     * @param integer $page
     * @return PaginationInterface
     */
    public function setCurrentPage(int $page): PaginationInterface;

    /**
     * Gets the number of items displayed per page.
     *
     * @return integer
     */
    public function getItemsPerPage(): int;

    /**
     * Set the numer of items to display per page
     *
     * @param integer $limit
     * @return PaginationInterface
     */
    public function setItemsPerPage(int $limit): PaginationInterface;

    /**
     * Get current items
     *
     * @return iterable
     */
    public function getPageItems(): iterable;

    /**
     * @param iterable $items
     */
    public function setPageItems(?iterable $items): PaginationInterface;

    public function getPageRange(): int;

    public function setPageRange(int $range): PaginationInterface;

    public function getPageCount(): int;

    public function getCollection();

    public function setCollection($items): PaginationInterface;

    public function getCollectionTotal(): int;

    public function setCollectionTotal(?int $total = 0): PaginationInterface;

    public function getQueryPaginationParameters(): array;

    public function setQueryPaginationParameters(array $params): PaginationInterface;

    /**
     * @return mixed|null
     */
    public function getQueryPaginationParameter(string $name);

    public function setQueryPaginationParameter(string $key, $value): PaginationInterface;

    /**
     * @return mixed|null
     */
    public function getPaginatorOption(string $name);

    /**
     * @return array<string, mixed>
     */
    public function getPaginatorOptions(): ?array;

    public function setPaginatorOptions(array $options = []): PaginationInterface;

    /**
     * @return array<string, mixed>
     */
    public function getPaginationData(): array;

    public function getTemplate(): ?string;

    public function setTemplate(?string $template): PaginationInterface;

    public function getSortableTemplate(): string;

    public function setSortableTemplate(?string $template): PaginationInterface;

    public function getFilterTemplate(): string;

    public function setFilterTemplate(?string $template): PaginationInterface;

    /**
     * @param string[]|string|null  $key
     * @param array                 $params
     * @return boolean
     */
    public function isSorted($key = null, array $params = []): bool;
}
