<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Pagination;

use Closure;
use Jeprodev\Paginator\Paginator;

/**
 * @todo: find a way to avoid exposing private member setters
 *
 * Sliding pagination
 */
final class SlidingPagination extends AbstractPagination
{
    /** @var string|null */
    private ?string $template = null;

    /**
     * @var string|null
     */
    private ?string $sortable_template = null;

    /**
     * @var string|null
     */
    private ?string $filter_template = null;

    /**
     * Closure which is executed to render pagination
     */
    public ?Closure $renderer = null;

    public function __construct(?array $params = [])
    {
        parent::__construct($params);
    }

    public function getSortField(): ?string
    {
        return $this->query_pagination_parameters[$this->getPaginatorOption(Paginator::SORT_FIELD_PARAMETER_NAME)] ?? null;
    }

    public function getDirection(): ?string
    {
        return $this->query_pagination_parameters[$this->getPaginatorOption(Paginator::SORT_DIRECTION_PARAMETER_NAME)] ?? null;
    }

    public function getPaginationData(): array
    {
        $pageCount = $this->getPageCount();

        $currentPage = $this->getCurrentPage();

        if ($pageCount < $currentPage) {
            $this->setCurrentPage($pageCount);
            $currentPage = $pageCount;
        }

        if ($this->getPageRange() > $pageCount) {
            $this->setPageRange($pageCount);
        }

        $delta = ceil($this->getPageRange() / 2);

        if (($currentPage - $delta) > ($pageCount - $this->getPageRange())) {
            $pages = range($pageCount - $this->getPageRange() + 1, $pageCount);
        } else {
            if (($currentPage - $delta) < 0) {
                $delta = $currentPage;
            }

            $offset = $currentPage - $delta;
            $pages = range($offset + 1, $offset + $this->getPageRange());
        }

        $proximity = floor($this->getPageRange() / 2);

        $startPage = $currentPage - $proximity;
        $endPage = $currentPage + $proximity;

        if ($startPage < 1) {
            $endPage = min($endPage + (1 - $startPage), $pageCount);
            $startPage = 1;
        }

        if ($endPage > $pageCount) {
            $startPage = max($startPage - ($endPage - $pageCount), 1);
            $endPage = $pageCount;
        }

        $viewData = [
            'current_page' => $currentPage,
            'items_per_page' => $this->getItemsPerPage(),
            'first' => 1,
            'first_page' => 1,
            'page_count' => $pageCount,
            'collection_total' => $this->getCollectionTotal(),
            'page_items_count' => count($this->getPageItems()),
            'page_range' => $this->getPageRange(),
            'start_page' => intval($startPage),
            'end_page' => intval($endPage)
        ];

        $paginatorOptions = $this->processPageParameters($this->getPaginatorOptions());
        $customOptions = $this->getQueryPaginationParameters();

        $viewData = array_merge($viewData, $paginatorOptions, $customOptions);

        if ($currentPage - 1 > 0) {
            $viewData['previous_page'] = $currentPage - 1;
        }

        if ($currentPage + 1 <= $pageCount) {
            $viewData['next_page'] = $currentPage + 1;
        }

        $viewData['pages_in_range'] = $pages;
        $viewData['first_page_in_range'] = min($pages);
        $viewData['last_page_in_range'] = max($pages);

        $viewData['first_item_number'] = 1;

        if ($this->getPageItems() !== null) {
            $viewData['page_items_count'] = count($this->getPageItems());
            $viewData['first_item_number'] = (($currentPage - 1) * $this->getItemsPerPage()) + 1;
            $viewData['last_item_number'] = $viewData['first_item_number'] + $viewData['page_items_count'] - 1;
        }

        return $viewData;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(?string $template): PaginationInterface
    {
        $this->template = $template;
        return $this;
    }

    public function getFilterTemplate(): string
    {
        return $this->filter_template;
    }

    public function setFilterTemplate(?string $template): PaginationInterface
    {
        $this->filter_template = $template;
        return $this;
    }

    public function getSortableTemplate(): string
    {
        return $this->sortable_template;
    }

    public function setSortableTemplate(?string $template): PaginationInterface
    {
        $this->sortable_template = $template;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isSorted($key = null, array $params = []): bool
    {
        $params = array_merge($this->getQueryPaginationParameters(), $params);

        if (null === $key) {
            return isset($params[$this->getPaginatorOption(Paginator::SORT_FIELD_PARAMETER_NAME)]);
        }

        if (is_array($key)) {
            $key = implode('+', $key);
        }

        $fieldKey = $params[$this->getPaginatorOption(Paginator::SORT_FIELD_PARAMETER_NAME)];

        return isset($fieldKey) &&  $fieldKey === $key;
    }

    private function processPageParameters(array $params): array
    {
        $parameters = [];

        foreach ($params as $key => $value) {
            $keys = explode('.', $key);
            $keySize = count($keys);

            if (!isset($parameters[$keys[0]])) {
                $parameters[$keys[0]] = [];
            }

            if (!isset($parameters[$keys[0]][$keys[1]])) {
                if ($keySize == 2) {
                    $parameters[$keys[0]][$keys[1]] = $value;
                } else {
                    $parameters[$keys[0]][$keys[1]] = [];
                }
            }

            if ($keySize == 3 && !isset($parameters[$keys[0]][$keys[1]][$keys[2]])) {
                $parameters[$keys[0]][$keys[1]][$keys[2]] = $value;
            }
        }

        return $parameters;
    }
}
