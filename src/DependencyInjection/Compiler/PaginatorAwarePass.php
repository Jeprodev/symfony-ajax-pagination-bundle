<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\DependencyInjection\Compiler;

use InvalidArgumentException;
use ReflectionClass;
use Jeprodev\Paginator\Definition\PaginatorAwareInterface;
use Symfony\Component\Config\Definition\Exception\InvalidDefinitionException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class PaginatorAwarePass implements CompilerPassInterface
{
    /**
     * @var string
     */
    public const PAGINATOR_AWARE_TAG = 'ajax_paginator.injectable';

    /**
     * @var string
     */
    public const PAGINATOR_AWARE_INTERFACE = PaginatorAwareInterface::class;

    /**
     * Populates all tagged services with the paginator service.
     *
     * @param ContainerBuilder $container
     * @return void
     *
     * @throws InvalidArgumentException
     * @throws InvalidDefinitionException
     */
    public function process(ContainerBuilder $container): void
    {
        $defaultAttributes = ['paginator' => 'ajax_paginator'];

        foreach ($container->findTaggedServiceIds(self::PAGINATOR_AWARE_TAG) as $key => [$attributes]) {
            $definition = $container->getDefinition($key);
            $defClass = $definition->getClass();

            if (null == $defClass) {
                throw new InvalidArgumentException(sprintf('Service "%s" not found.', $key));
            }

            /** @var class-string $defClass */
            $refClass = new ReflectionClass($defClass);
            if (!$refClass->implementsInterface(self::PAGINATOR_AWARE_INTERFACE)) {
                throw new InvalidArgumentException(sprintf('Service "%s" must implement interface "%s".', $key, self::PAGINATOR_AWARE_INTERFACE));
            }

            $attributes = array_merge($defaultAttributes, $attributes);

            if (!$container->has($attributes['paginator'])) {
                $message = 'Paginator service "%s" for tag "%s" on service "%s" could not be found.';
                throw new InvalidDefinitionException(sprintf($message, $attributes['paginator'], self::PAGINATOR_AWARE_TAG, $key));
            }

            $definition->addMethodCall('setPaginator', [new Reference($attributes['paginator'])]);
            $container->setDefinition($key, $definition);
        }
    }
}
