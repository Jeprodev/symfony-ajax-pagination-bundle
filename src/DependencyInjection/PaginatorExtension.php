<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class PaginatorExtension extends Extension
{
    /**
     * Build the extension services.
     *
     * @param array<string, array<string, mixed>> $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $processor = new Processor();

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yaml');

        if ($container->hasParameter('templating.engines')) {
            $engines = $container->getParameter('templating.engines');

            if (in_array('php', $engines, true)) {
                $loader->load('templating.yaml');
            }
        }

        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        $container->setParameter('ajax_paginator.template.pagination', $config['template']['pagination']);
        $container->setParameter('ajax_paginator.template.filtration', $config['template']['filtration']);
        $container->setParameter('ajax_paginator.template.sortable', $config['template']['sortable']);
        $container->setParameter('ajax_paginator.filter.field', $config['default_options']['filter']['field']);
        $container->setParameter('ajax_paginator.filter.value', $config['default_options']['filter']['value']);
        $container->setParameter('ajax_paginator.sort.field', $config['default_options']['sort']['field']);
        $container->setParameter('ajax_paginator.sort.direction', $config['default_options']['sort']['direction']);
        $container->setParameter('ajax_paginator.pagination.name', $config['default_options']['pagination']['name']);
        $container->setParameter('ajax_paginator.pagination.out_of_range', $config['default_options']['pagination']['out_of_range']);
        $container->setParameter('ajax_paginator.pagination.range', $config['default_options']['pagination']['range']);
        $container->setParameter('ajax_paginator.pagination.limit', $config['default_options']['pagination']['limit']);
        $container->setParameter('ajax_paginator.distinct', $config['default_options']['distinct']);

        $definition = $container->getDefinition('ajax_paginator');
        $definition->addMethodCall('setDefaultPaginatorOptions', [[
            'ajax_paginator.pagination.name' => $config['default_options']['pagination']['name'],
            'ajax_paginator.pagination.range' => $config['default_options']['pagination']['range'],
            'ajax_paginator.pagination.limit' => $config['default_options']['pagination']['limit'],
            'ajax_paginator.pagination.out_of_range' => $config['default_options']['pagination']['out_of_range'],
            'ajax_paginator.filter.field' => $config['default_options']['filter']['field'],
            'ajax_paginator.filter.value' => $config['default_options']['filter']['value'],
            'ajax_paginator.sort.field' => $config['default_options']['sort']['field'],
            'ajax_paginator.sort.direction' => $config['default_options']['sort']['direction'],
            'ajax_paginator.template.pagination' => $config['template']['pagination'],
            'ajax_paginator.template.filtration' => $config['template']['filtration'],
            'ajax_paginator.template.sortable' => $config['template']['sortable'],
            'ajax_paginator.distinct' => $config['default_options']['distinct']
        ]]);
    }
}
