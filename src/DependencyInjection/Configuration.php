<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\DependencyInjection;

use Jeprodev\Paginator\PaginatorInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder('ajax_paginator');
        $rootNode = $builder->getRootNode();

        $rootNode->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('default_options')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('sort')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('field')->defaultValue('sort')->end()
                                ->scalarNode('direction')->defaultValue('direction')->end()
                            ->end()
                        ->end()
                        ->arrayNode('filter')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('field')->defaultValue('field')->end()
                                ->scalarNode('value')->defaultValue('value')->end()
                            ->end()
                        ->end()
                        ->arrayNode('pagination')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('name')->defaultValue('page')->end()
                                ->scalarNode('range')->defaultValue(5)->end()
                                ->scalarNode('out_of_range')->defaultValue(PaginatorInterface::PAGE_OUT_OF_RANGE_IGNORE)->end()
                                ->scalarNode('limit')->defaultValue(PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT_VALUE)->end()
                            ->end()
                        ->end()
                        ->booleanNode('distinct')->defaultTrue()->end()
                    ->end()
                ->end()
                ->arrayNode('template')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('pagination')->defaultValue('@Paginator/pagination.html.twig')->end()
                        ->scalarNode('filtration')->defaultValue('@Paginator/filter.html.twig')->end()
                        ->scalarNode('sortable')->defaultValue('@Paginator/sort.html.twig')->end()
                    ->end()
                ->end()
            ->end();

        return $builder;
    }
}
