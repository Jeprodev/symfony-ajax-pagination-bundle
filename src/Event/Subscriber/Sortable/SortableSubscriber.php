<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Event
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Sortable;

use Jeprodev\Paginator\Event\PaginatorBeforeEvent;
use Jeprodev\Paginator\Event\Subscriber\Sortable\ArraySubscriber;
use Jeprodev\Paginator\Event\Subscriber\Sortable\PropelQuerySubscriber;
use Jeprodev\Paginator\Event\Subscriber\Sortable\SolariumQuerySubscriber;
use Jeprodev\Paginator\Event\Subscriber\Sortable\Doctrine\ORM\QuerySubscriber as DoctrineOrmQuerySubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SortableSubscriber implements EventSubscriberInterface
{
    private bool $loaded = false;

    public function before(PaginatorBeforeEvent $event): void
    {
        if (!$this->loaded) {
            /** @var EventDispatcherInterface $dispatcher */
            $dispatcher = $event->getEventDispatcher();

            $request = $event->getRequest();

            $dispatcher->addSubscriber(new DoctrineORMQuerySubscriber($request));
            //$dispatcher->addSubscriber(new Doctrine\ODM\MongoDB\QuerySubscriber($request));
            //$dispatcher->addSubscriber(new ElasticaQuerySubscriber($request));
            $dispatcher->addSubscriber(new PropelQuerySubscriber($request));
            $dispatcher->addSubscriber(new SolariumQuerySubscriber($request));
            $dispatcher->addSubscriber(new ArraySubscriber($request));

            $this->loaded = true;
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.before' => ['before', 1]
        ];
    }
}
