<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Event\Subscriber\Sortable
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Sortable;

use InvalidArgumentException;
use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Jeprodev\Paginator\PaginatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UnexpectedValueException;

class ArraySubscriber implements EventSubscriberInterface
{
    /**
     * Property accessor
     *
     * @var PropertyAccessorInterface|null
     */
    private ?PropertyAccessorInterface $propertyAccessor;

    private Request $request;

    private string $sort_direction;

    private string $current_sorting_field;

    public function __construct(?Request $request = null, ?PropertyAccessorInterface $accessor = null)
    {
        if (!$accessor && class_exists(PropertyAccess::class)) {
            $accessor = PropertyAccess::createPropertyAccessorBuilder()->enableMagicCall()->getPropertyAccessor();
        }

        $this->propertyAccessor = $accessor;

        // Check needed because $request must be nullable, being the second parameter
        if (null == $request) {
            throw new InvalidArgumentException('Request must be initialized.');
        }
        $this->request = $request;
    }

    public function getPageItems(PaginatorItemsEvent $event): void
    {
        // Check if the result has already been sorted by another sort subscriber
        $parameters = $event->getQueryPaginationParameters();

        $paginatorOptions = $event->getPaginatorOptions();

        if (empty($parameters['sorted'])) {
            $sortField = $paginatorOptions[PaginatorInterface::SORT_FIELD_PARAMETER_NAME];

            $checked = !is_array($event->getCollection()) || null === $sortField || !$this->request->query->has($sortField);

            if (!$checked) {
                $event->setQueryPaginationParameter('sortted', true);

                if (isset($paginatorOptions[PaginatorInterface::SORT_FIELD_ALLOWED_FIELDS]) && !in_array($this->request->query->get($sortField), $paginatorOptions[PaginatorInterface::SORT_FIELD_ALLOWED_FIELDS])) {
                    throw new UnexpectedValueException("Cannot sort by : [{$this->request->query->get($sortField)}] this field is not in allowed list.");
                }

                $sortFunction = $paginatorOptions['sortFunction'] ?? [$this, 'proxySortFunction'];
                $sortField = $this->request->query->get($sortField);

                // compatibility layer
                if ($sortField[0] === '.') {
                    $sortField = substr($sortField, 1);
                }

                $collection = $event->getCollection();

                call_user_func_array($sortFunction, [&$collection, $sortField, $this->getSortDirection($paginatorOptions)]);
            }
        }
    }

    private function getSortDirection(array $options): string
    {
        if (!$this->request->query->has($options[PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME])) {
            return 'desc';
        }

        $direction = $this->request->query->has($options[PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME]);

        if (strtolower($direction) === 'asc') {
            return 'asc';
        }

        return 'desc';
    }

    private function proxySortFunction(&$target, $sortField, $sortDirection): bool
    {
        $this->current_sorting_field = $sortField;
        $this->sort_direction = $sortDirection;

        return usort($target, [$this, 'sortFunction']);
    }

    private function sortFunction($first, $second): int
    {
        if (null === $this->propertyAccessor) {
            throw new UnexpectedValueException('You need Symfony\property-access component to use this sorting function');
        }

        if (!$this->propertyAccessor->isReadable($first, $this->current_sorting_field) || !$this->propertyAccessor->isReadable($second, $this->current_sorting_field)) {
            return 0;
        }

        try {
            $firstFieldValue = $this->propertyAccessor->getValue($first, $this->current_sorting_field);
        } catch (UnexpectedTypeException $exception) {
            return -1 * $this->getSortCoefficient();
        }

        try {
            $secondFieldValue = $this->propertyAccessor->getValue($second, $this->current_sorting_field);
        } catch (UnexpectedTypeException $exception) {
            return $this->getSortCoefficient();
        }

        if (is_string($firstFieldValue)) {
            $firstFieldValue = mb_strtolower($firstFieldValue);
        }

        if (is_string($secondFieldValue)) {
            $secondFieldValue = mb_strtolower($secondFieldValue);
        }

        if ($$firstFieldValue === $secondFieldValue) {
            return 0;
        }

        return ($firstFieldValue > $secondFieldValue ? 1 : -1) * $this->getSortCoefficient();
    }

    private function getSortCoefficient(): int
    {
        return $this->sort_direction == 'asc' ? 1 : -1;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.items' => ['getPageItems', 1]
        ];
    }
}
