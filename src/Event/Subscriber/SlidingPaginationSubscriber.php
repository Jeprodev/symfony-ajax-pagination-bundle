<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Event\Subscriber\Sliding
 * @link            http://jeprodev.net
 *
 * @copyright (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber;

use Jeprodev\Paginator\Event\PaginationEvent;
use Jeprodev\Paginator\Pagination\SlidingPagination;
use Jeprodev\Paginator\Paginator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class SlidingPaginationSubscriber implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $route;

    /**
     * @var array
     */
    private $query_pagination_parameters = [];

    private $paginator_options;

    public function __construct(array $options)
    {
        $this->paginator_options = $options;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (HttpKernelInterface::MAIN_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();

        $this->route = $request->attributes->get('_route');
        $this->query_pagination_parameters = array_replace($request->query->all(), $request->attributes->get('_route_params', []));

        foreach ($this->query_pagination_parameters as $key => $param) {
            if (str_starts_with($key, '_')) {
                unset($this->query_pagination_parameters[$key]);
            }
        }
    }

    public function getPagination(PaginationEvent $event): void
    {
        $options = $event->paginator_options;

        if (isset($options[Paginator::DEFAULT_SORT_FIELD_NAME]) && !isset($this->query_pagination_parameters[$options[Paginator::SORT_FIELD_PARAMETER_NAME]])) {
            $this->query_pagination_parameters[$options[Paginator::SORT_FIELD_PARAMETER_NAME]] = $options[Paginator::DEFAULT_SORT_FIELD_NAME];
        }

        if (isset($options[Paginator::DEFAULT_SORT_DIRECTION]) && !isset($this->query_pagination_parameters[$options[Paginator::SORT_DIRECTION_PARAMETER_NAME]])) {
            $this->query_pagination_parameters[$options[Paginator::SORT_DIRECTION_PARAMETER_NAME]] = $options[Paginator::DEFAULT_SORT_DIRECTION];
        }

        // remove default sort params from pagination links.
        if (isset($options[Paginator::REMOVE_DEFAULT_SORT_PARAMS]) && true === $options[Paginator::REMOVE_DEFAULT_SORT_PARAMS]) {
            $defaultSortFieldName = $options[Paginator::DEFAULT_SORT_FIELD_NAME];
            $sortFieldParameterName = $this->query_pagination_parameters[$options[Paginator::SORT_FIELD_PARAMETER_NAME]];
            $isFieldsEqual = $defaultSortFieldName === $sortFieldParameterName;

            $defaultSortDirection = $options[Paginator::DEFAULT_SORT_DIRECTION];
            $sortDirectionParameterName = $this->query_pagination_parameters[$options[Paginator::SORT_DIRECTION_PARAMETER_NAME]];
            $isDirectionEqual = $defaultSortDirection === $sortDirectionParameterName;

            if (isset($defaultSortFieldName, $sortFieldParameterName, $defaultSortDirection, $sortDirectionParameterName) && $isFieldsEqual && $isDirectionEqual) {
                unset($this->query_pagination_parameters[$options[Paginator::SORT_FIELD_PARAMETER_NAME]], $this->query_pagination_parameters[$options[Paginator::SORT_DIRECTION_PARAMETER_NAME]]);
            }
        }

        $pagination = new SlidingPagination($this->query_pagination_parameters);

        $pagination->setPageRoute($this->route);

        if (isset($this->paginator_options[Paginator::PAGINATOR_PAGINATION_TEMPLATE])) {
            $pagination->setTemplate($this->paginator_options[Paginator::PAGINATOR_PAGINATION_TEMPLATE]);
        }

        if (isset($this->paginator_options[Paginator::PAGINATOR_SORTABLE_TEMPLATE])) {
            $pagination->setSortableTemplate($this->paginator_options[Paginator::PAGINATOR_SORTABLE_TEMPLATE]);
        }

        if (isset($this->paginator_options[Paginator::PAGINATOR_FILTRATION_TEMPLATE])) {
            $pagination->setFilterTemplate($this->paginator_options[Paginator::PAGINATOR_FILTRATION_TEMPLATE]);
        }

        if (isset($this->paginator_options[Paginator::DEFAULT_PAGE_ITEMS_LIMIT])) {
            $pagination->setItemsPerPage($this->paginator_options[Paginator::DEFAULT_PAGE_ITEMS_LIMIT]);
        }

        if (isset($this->paginator_options[Paginator::DEFAULT_PAGE_RANGE])) {
            $pagination->setPageRange($this->paginator_options[Paginator::DEFAULT_PAGE_RANGE]);
        }

        $event->setPagination($pagination);
        $event->stopPropagation();
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pagination' => ['getPagination', 1]
        ];
    }
}
