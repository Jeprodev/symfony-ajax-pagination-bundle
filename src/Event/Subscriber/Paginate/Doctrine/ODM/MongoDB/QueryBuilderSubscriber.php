<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate\Doctrine\ODM\MongoDB;

use Doctrine\ODM\MongoDB\Query\Builder;
use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Jeprodev\Paginator\PaginatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class QueryBuilderSubscriber implements EventSubscriberInterface
{
    public function getPageItems(PaginatorItemsEvent $event): void
    {
        /*todo if ($event->getCollection() instanceof Builder) {
            $event->setCollection(
                $event->getCollection()
                ->getQuery($event->options[PaginatorInterface::ODM_QUERY_OPTIONS] ?? [])
            );
        }*/
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.items' => ['getPageItems', 10]
        ];
    }
}
