<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate\Doctrine\ODM\MongoDB;

//use Doctrine\ODM\MongoDB\Query\Query;
use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use ReflectionClass;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use UnexpectedValueException;

class QuerySubscriber implements EventSubscriberInterface
{
    public function getPageItems(PaginatorItemsEvent $event): void
    {
        /*todo if ($event->getCollection() instanceof Query) {
            $type = $event->getCollection()->getType();

            if ($type !== Query::TYPE_FIND) {
                throw new UnexpectedValueException('ODM query must be a find type query');
            }

            static $reflectionProperty;

            if (is_null($reflectionProperty)) {
                $reflectionClass = new ReflectionClass('Doctrine\ODM\MongoDB\Query\Query');
                $reflectionProperty = $reflectionClass->getProperty('query');
                $reflectionProperty->setAccessible(true);
            }

            $queryOptions = $reflectionProperty->getValue($event->getCollection());
            $resultCount = clone $event->getCollection();
            $queryOptions['type'] = Query::TYPE_COUNT;
            $reflectionProperty->setValue($resultCount, $queryOptions);
            $event->setPageItemsCount($resultCount->execute());

            $queryOptions = $reflectionProperty->getValue($event->getCollection());
            $queryOptions['type'] = Query::TYPE_FIND;
            $queryOptions['limit'] = $event->getItemsPerPage();
            $queryOptions['skip'] = $event->getOffset();

            $resultQuery = clone $event->getCollection();
            $reflectionProperty->setValue($resultQuery, $queryOptions);
            $cursor = $resultQuery->execute();

            $event->page_items = [];

            foreach ($cursor as $item) {
                $event->page_items[] = $item;
            }
        }*/
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.items' => ['getPageItems', 0]
        ];
    }
}
