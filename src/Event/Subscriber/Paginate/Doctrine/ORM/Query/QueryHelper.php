<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate\Doctrine\ORM\Query;

use Doctrine\ORM\Query;

/**
 * ORM Query helper for cloning and hint processing
 */
class QueryHelper
{
    /**
     * Clones the given $query and copies all used parameters and hints
     *
     * @param Query $query
     * @return Query
     */
    public static function cloneQuery(Query $query): Query
    {
        $cloneQuery = clone $query;
        $cloneQuery->setParameters(clone $query->getParameters());

        foreach ($query->getHints() as $name => $hint) {
            $cloneQuery->setHint($name, $hint);
        }

        return $cloneQuery;
    }

    /**
     * Add a custom TreeWalker $walker class name to be included in the CustomTreeWalker hint list
     * of the given $query
     *
     * @param Query $query
     * @param string $walker
     * @return void
     */
    public static function addCustomTreeWalker(Query $query, string $walker): void
    {
        $customTreeWalkers = $query->getHint(Query::HINT_CUSTOM_TREE_WALKERS);

        if ($customTreeWalkers !== false && is_array($customTreeWalkers)) {
            $customTreeWalkers = array_merge($customTreeWalkers, [$walker]);
        } else {
            $customTreeWalkers = [$walker];
        }

        $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, $customTreeWalkers);
    }
}
