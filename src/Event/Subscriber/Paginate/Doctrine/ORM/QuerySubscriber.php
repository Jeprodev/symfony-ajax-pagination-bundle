<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate\Doctrine\ORM;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\{CountWalker, Paginator};
use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Jeprodev\Paginator\PaginatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class QuerySubscriber implements EventSubscriberInterface
{
    public const HINT_COUNT = 'ajax_paginator.pager.count';

    public const HINT_FETCH_JOIN_COLLECTION = 'ajax_paginator.pager.fetch_join_collection';

    public function getPageItems(PaginatorItemsEvent $event): void
    {
        if ($event->getCollection() instanceof Query) {
            $event->stopPropagation();

            $useOutputWalkers = false;

            $paginatorOptions = $event->getPaginatorOptions();

            if (isset($paginatorOptions['wrap-queries'])) {
                $useOutputWalkers = $paginatorOptions['wrap_options'];
            }

            $event->getCollection()->setFirstResult($event->getOffset())
                ->setMaxResults($event->getItemsPerPage())
                ->setHint(CountWalker::HINT_DISTINCT, $paginatorOptions[PaginatorInterface::DISTINCT]);

            $fetchJoinCollection = true;

            if ($event->getCollection()->hasHint(self::HINT_FETCH_JOIN_COLLECTION)) {
                $fetchJoinCollection = $event->getCollection()->getHint(self::HINT_FETCH_JOIN_COLLECTION);
            } elseif (isset($event->options[PaginatorInterface::DISTINCT])) {
                $fetchJoinCollection = $paginatorOptions[PaginatorInterface::DISTINCT];
            }

            $paginator = new Paginator($event->getCollection(), $fetchJoinCollection);
            $paginator->setUseOutputWalkers($useOutputWalkers);

            if (($count = $event->getCollection()->getHint(self::HINT_COUNT)) !== false) {
                $event->setCollectionTotal((int)$count);
            } else {
                $event->setCollectionTotal(count($paginator));
            }

            $event->setPageItems(iterator_to_array($paginator));
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.items' => ['getPageItems', 0]
        ];
    }
}
