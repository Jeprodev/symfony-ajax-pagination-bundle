<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate\Callback;

class CallbackPaginator
{
    /**
     * @var callable
     */
    private $items_count;

    /**
     * @var callable
     */
    private $page_items;

    public function __construct(callable $count, callable $items)
    {
        $this->items_count = $count;
        $this->page_items = $items;
    }

    public function getPaginationCount(): int
    {
        return call_user_func($this->items_count);
    }

    public function getPaginationItems(int $offset, int $limit): array
    {
        return call_user_func($this->page_items, $offset, $limit);
    }
}
