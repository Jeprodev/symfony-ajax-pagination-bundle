<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate\Callback;

use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CallbackSubscriber implements EventSubscriberInterface
{
    public function getPageItems(PaginatorItemsEvent $event): void
    {
        if ($event->getCollection() instanceof CallbackPaginator) {
            $event->setPageItemsCount($event->getCollection()->getPaginationCount());

            if ($event->getCollectionTotal() > 0) {
                $event->setPageItems($event->getCollection()->getPaginationItems($event->getOffset(), $event->getItemsPerPage()));
            } else {
                $event->setPageItems([]);
            }
            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.items' => ['getPageItems', 0]
        ];
    }
}
