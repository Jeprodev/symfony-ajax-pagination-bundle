<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate;

/*use Jeprodev\Paginator\Event\Subscriber\Doctrine\ORM;
/*use Jeprodev\Paginator\Event\Subscriber\Doctrine\ORM;
/*use Jeprodev\Paginator\Event\Subscriber\Doctrine\ORM;

use Jeprodev\Paginator\Event\Subscriber\Doctrine\ORM;
use Jeprodev\Paginator\Event\Subscriber\Doctrine\ORM;*/

use Jeprodev\Paginator\Event\PaginationEvent;
use Jeprodev\Paginator\Event\PaginatorBeforeEvent;
use Jeprodev\Paginator\Pagination\SlidingPagination;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaginationSubscriber implements EventSubscriberInterface
{
    /**
     * Lazy-load state tracker
     *
     * @var boolean
     */
    private bool $loaded = false;

    public function beforePagination(PaginatorBeforeEvent $event): void
    {
        // Do not lazy-load more than once
        if (!$this->loaded) {
            /** @var EventDispatcherInterface $dispatcher */
            $dispatcher = $event->getEventDispatcher();

            $dispatcher->addSubscriber(new ArraySubscriber());
            $dispatcher->addSubscriber(new Callback\CallbackSubscriber());
            $dispatcher->addSubscriber(new Doctrine\CollectionSubscriber());
            $dispatcher->addSubscriber(new Doctrine\DBAL\QueryBuilderSubscriber());
            $dispatcher->addSubscriber(new Doctrine\ORM\QueryBuilderSubscriber());
            $dispatcher->addSubscriber(new Doctrine\ORM\QuerySubscriber());
            $dispatcher->addSubscriber(new Doctrine\ODM\MongoDB\QueryBuilderSubscriber());
            $dispatcher->addSubscriber(new Doctrine\ODM\MongoDB\QuerySubscriber());
            $dispatcher->addSubscriber(new Doctrine\ODM\PHPCR\QueryBuilderSubscriber());
            $dispatcher->addSubscriber(new Doctrine\ODM\PHPCR\QuerySubscriber());
            $dispatcher->addSubscriber(new PropelQuerySubscriber());
            $dispatcher->addSubscriber(new SolariumQuerySubscriber());
            $dispatcher->addSubscriber(new ElasticaQuerySubscriber());

            $this->loaded = true;
        }
    }

    public function getPagination(PaginationEvent $event)
    {
        $event->setPagination(new SlidingPagination());
        $event->stopPropagation();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.before' => ['beforePagination', 0],
            'ajax_paginator.pagination' => ['getPagination', 0]
        ];
    }
}
