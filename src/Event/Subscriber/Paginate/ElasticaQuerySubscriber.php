<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event\Subscriber\Paginate;

use Elastica\Query;
use Elastica\SearchableInterface;
use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ElasticaQuerySubscriber implements EventSubscriberInterface
{
    public function getPageItems(PaginatorItemsEvent $event): void
    {
        $collection = $event->getCollection();

        if (is_array($collection) && count($collection) == 2 && reset($collection) instanceof SearchableInterface && end($collection) instanceof Query) {
            [$searchable, $query] = $collection;

            $query->setFrom($event->getOffset());
            $query->setSize($event->getItemsPerPage());

            $results = $searchable->search($query);

            $event->setCollectionTotal($results->getTotalHits());

            if ($results->hasAggregations()) {
                $event->setQueryPaginationParameter('aggregations', $results->getAggregations());
            }

            $event->setQueryPaginationParameter('resultSet', $results)
                ->setPageItems($results->getResults())->stopPropagation();
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.pager.items' => ['getPageItems', 0]
        ];
    }
}
