<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Paginator\Event
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event;

use Symfony\Contracts\EventDispatcher\Event;

class PaginatorEvent extends Event
{
    /**
     * A target being paginated
     *
     * @var mixed
     */
    private $collection;

    /**
     * Count result
     */
    private int $collection_total = 0;

    /**
     * List of options
     *
     * @var array
     */
    private array $paginator_options;

    public function getCollection()
    {
        return $this->collection;
    }

    public function setCollection(&$target): self
    {
        $this->collection = $target;
        if ($this->collection !== null) {
            $this->collection_total = count($this->collection);
        }
        return $this;
    }

    public function getCollectionTotal(): int
    {
        return $this->collection_total;
    }

    public function setCollectionTotal(int $total): self
    {
        $this->collection_total = $total;
        return $this;
    }

    public function getPaginatorOptions(): ?array
    {
        return $this->paginator_options;
    }

    public function setPaginatorOptions(array &$options): self
    {
        $this->paginator_options = $options;
        return $this;
    }
}
