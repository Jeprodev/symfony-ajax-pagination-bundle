<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Paginator\Event
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event;

use Countable;
use Jeprodev\Paginator\PaginatorInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Specific Event class for paginator
 */
class PaginatorItemsEvent extends PaginatorEvent
{
    /**
     * Items result for a page
     *
     * @var mixed
     */
    private $page_items;

    /**
     * Count result
     */
    private int $page_items_count;

    private int $offset;

    private int $items_per_page = PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT_VALUE;

    private array $query_pagination_Parameters = [];

    public function __construct(int $offset, $limit)
    {
        $this->offset = $offset;
        $this->items_per_page = $limit;
    }

    public function getQueryPaginationParameters(): array
    {
        return $this->query_pagination_Parameters;
    }

    public function setQueryPaginationParameter($name, $value): self
    {
        $this->query_pagination_Parameters[$name] = $value;
        return $this;
    }

    public function unsetQueryPaginationParameter($name): self
    {
        if (isset($this->query_pagination_Parameters[$name])) {
            unset($this->query_pagination_Parameters[$name]);
        }
        return $this;
    }

    public function getPageItemsCount(): int
    {
        return $this->page_items_count;
    }

    public function setPageItemsCount(int $count): self
    {
        $this->page_items_count = $count;
        return $this;
    }

    public function getItemsPerPage(): int
    {
        return $this->items_per_page;
    }

    public function setItemsPerPage(int $value): self
    {
        $this->items_per_page = $value;
        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getPageItems(): ?array
    {
        return $this->page_items;
    }

    public function setPageItems($items): self
    {
        $this->page_items = $items;

        if ($items instanceof Countable || is_array($items)) {
            $this->setPageItemsCount(count($items));
        }

        return $this;
    }
}
