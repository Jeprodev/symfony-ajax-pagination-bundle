<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      Paginator\Event
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Specific Event class for paginator
 */
class PaginatorBeforeEvent extends Event
{
    private EventDispatcherInterface $eventDispatcher;

    private ?Request $request;

    public function __construct(EventDispatcherInterface $dispatcher, ?Request $request)
    {
        $this->eventDispatcher = $dispatcher;
        $this->request = $request;
    }

    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    public function getRequest(): ?Request
    {
        return $this->request ?? Request::createFromGlobals();
    }
}
