<?php

/**
 * @version         1.0.0
 * @package         Jeprodev\Paginator
 * @subpackage      DependencyInjection
 * @link            http://jeprodev.net
 *
 * @copyright       (C)   2009 - 2011
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Jeprodev\Paginator;

use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Jeprodev\Paginator\Pagination\PaginationInterface;
use Jeprodev\Paginator\Exception\PageNumberOutOfRangeException;
use Jeprodev\Paginator\Event\Subscriber\Sortable\SortableSubscriber;
use Jeprodev\Paginator\Event\Subscriber\Paginate\PaginationSubscriber;
use Jeprodev\Paginator\Event\{PaginatorAfterEvent, PaginatorBeforeEvent, PaginatorItemsEvent, PaginationEvent};
use Jeprodev\Paginator\Exception\InvalidPageLimitException;
use Jeprodev\Paginator\Exception\InvalidPageNumberException;

/**
 * Paginator uses event dispatcher to trigger pagination lifecycle events.
 * Subscribers are expected to paginate wanted target and finally it generates
 * pagination view which is only the result of paginator.
 */
class Paginator implements PaginatorInterface
{
    /**
     * Default Options of paginator
     *
     * @var array<string, string|int|bool>
     */
    private array $default_options = [
        self::PAGE_PARAMETER_NAME => 'page',
        self::SORT_FIELD_PARAMETER_NAME => 'sort',
        self::SORT_DIRECTION_PARAMETER_NAME => 'direction',
        self::FILTER_FIELD_PARAMETER_NAME => 'param',
        self::FILTER_VALUE_PARAMETER_NAME => 'value',
        self::PAGE_OUT_OF_RANGE_BEHAVIOR => self::PAGE_OUT_OF_RANGE_IGNORE,
        self::DEFAULT_PAGE_ITEMS_LIMIT => self::DEFAULT_PAGE_ITEMS_LIMIT_VALUE,
        self::DISTINCT => true,
        self::USE_AJAX => false,
        self::USE_AJAX_PARAMETERS => [],
        self::USE_AJAX_ROUTE_PARAMETER => ''
    ];

    private ?EventDispatcherInterface $eventDispatcher;

    private ?RequestStack $requestStack;

    public function __construct(?EventDispatcherInterface $dispatcher = null, ?RequestStack $stack = null)
    {
        $this->eventDispatcher = $dispatcher;

        if (is_null($this->eventDispatcher)) {
            $this->eventDispatcher = new EventDispatcher();
            $this->eventDispatcher->addSubscriber(new PaginationSubscriber());
            $this->eventDispatcher->addSubscriber(new SortableSubscriber());
        }

        $this->requestStack = $stack;
    }

    /**
     * Override the default paginator options to be reused for paginations
     */
    public function setDefaultPaginatorOptions(array $options)
    {
        $this->default_options = array_merge($this->default_options, $options);
    }

    /**
     * {@inheritDoc}
     */
    public function paginate($collection, int $page = 1, int $limit = PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT_VALUE, array $options = []): ?PaginationInterface
    {
        if ($collection === null) {
            throw new RuntimeException("Invalid collection, paginator can't be applied on null collection of items.");
        }

        if ($page <= 0) {
            throw new InvalidPageNumberException($page);
        }

        $limit = $limit ?? PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT_VALUE;

        if ($limit <= 0) {
            throw new InvalidPageLimitException($limit);
        }

        $offset = ($page - 1) * $limit;
        $options = array_merge($this->default_options, $options);

        if ($options[self::DEFAULT_PAGE_ITEMS_LIMIT] !== $limit) {
            $options[self::DEFAULT_PAGE_ITEMS_LIMIT] = $limit;
        }

        // normalize sort field
        if (isset($options[self::DEFAULT_SORT_FIELD_NAME]) && is_array($options[self::DEFAULT_SORT_FIELD_NAME])) {
            $options[self::DEFAULT_SORT_FIELD_NAME] = implode('+', $options[self::DEFAULT_SORT_FIELD_NAME]);
        }

        /*/ normalize sort field
        if (isset($options[self::SORT_FIELD_PARAMETER_NAME]) && is_array($options[self::SORT_FIELD_PARAMETER_NAME])) {
            $options[self::SORT_FIELD_PARAMETER_NAME] = implode('+', $options[self::SORT_FIELD_PARAMETER_NAME]);
        }*/

        $request = (null == $this->requestStack) ? (Request::createFromGlobals()) : $this->requestStack->getCurrentRequest();

        // Default sort field and direction are set base on options (is availabe)
        if (isset($options[self::DEFAULT_SORT_FIELD_NAME]) && !$request->query->has($options[self::SORT_FIELD_PARAMETER_NAME])) {
            $request->query->set($options[self::SORT_FIELD_PARAMETER_NAME], $options[self::DEFAULT_SORT_FIELD_NAME]);

            if (!$request->query->has($options[self::SORT_DIRECTION_PARAMETER_NAME])) {
                $request->query->set($options[self::SORT_DIRECTION_PARAMETER_NAME], $options[self::DEFAULT_SORT_DIRECTION] ?? 'ASC');
            }
        }

        // Before pagination start
        $beforeEvent = new PaginatorBeforeEvent($this->eventDispatcher, $request);
        $this->eventDispatcher->dispatch($beforeEvent, 'ajax_paginator.pager.before');

        // items
        $itemsEvent = new PaginatorItemsEvent($offset, $limit);
        $itemsEvent->setPaginatorOptions($options)->setCollection($collection);

        $this->eventDispatcher->dispatch($itemsEvent, 'ajax_paginator.pager.items');

        if (!$itemsEvent->isPropagationStopped()) {
            throw new RuntimeException('One of listeners must count and slice given collection');
        }

        if ($page > ceil($itemsEvent->getCollectionTotal()  / $limit)) {
            $pageOutOfRangeOption = $options[self::PAGE_OUT_OF_RANGE_BEHAVIOR] ?? $this->default_options[self::PAGE_OUT_OF_RANGE_BEHAVIOR];

            if ($pageOutOfRangeOption === self::PAGE_OUT_OF_RANGE_FIX && $itemsEvent->getCollectionTotal() > 0) {
                // replace page number out of range with max page
                return $this->paginate($collection, (int)ceil($itemsEvent->getCollectionTotal() / $limit), $limit, $options);
            }

            if ($pageOutOfRangeOption === self::PAGE_OUT_OF_RANGE_THROW_EXCEPTION && $page > 0) {
                throw new PageNumberOutOfRangeException(
                    sprintf('Page number: %d is out of range.', $page),
                    (int)ceil($itemsEvent->getCollectionTotal() / $limit)
                );
            }
        }

        //pagination initalization event
        $paginationEvent = new PaginationEvent();
        $paginationEvent->collection = &$collection;
        $paginationEvent->paginator_options = &$options;

        $this->eventDispatcher->dispatch($paginationEvent, 'ajax_paginator.pagination');

        if (!$paginationEvent->isPropagationStopped()) {
            throw new RuntimeException('One of listeners must create pagination view.');
        }

        // Pagination class can be different, with different rendering methods
        $pagination = $paginationEvent->getPagination();
        $pagination
            ->setQueryPaginationParameters($itemsEvent->getQueryPaginationParameters())
            ->setCurrentPage($page)
            ->setItemsPerPage($limit)
            ->setPaginatorOptions($options)
            ->setPageItems($itemsEvent->getPageItems())
            ->setCollectionTotal($itemsEvent->getCollectionTotal())
            ->setCollection($itemsEvent->getCollection());

        // After
        $afterEvent = new PaginatorAfterEvent($pagination);
        $this->eventDispatcher->dispatch($afterEvent, 'ajax_paginator.pager.after');

        return $pagination;
    }
}
