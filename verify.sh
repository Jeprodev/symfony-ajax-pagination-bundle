#!/bin/sh

clear 

php ./vendor/autoload.php

./local-php-security-checker  --path=./composer.lock

./vendor/bin/phpcs --colors --standard=PSR12 --exclude=Generic.Files.lineLength ./src/

vendor/bin/phpstan analyze -l 0 src tests

# todo php bin/console lint:twig

php -dxdebug.mode=coverage -d zend.enable_gc=0 vendor/bin/phpunit --testdox --coverage-html ./../../websites/build/coverage/html-coverage

