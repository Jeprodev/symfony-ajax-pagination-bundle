<?php 

namespace Jeprodev\Paginator\Tests\Mock;

use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Jeprodev\Paginator\Event\Subscriber\Paginate\ArraySubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CustomParameterSubscriber extends ArraySubscriber
{
    public static function getSubscribedEvents(): array
    {
        return [
            'ajax_paginator.items' => ['getPageItems', 10]
        ];
    }

    public function getPageItems(PaginatorItemsEvent $event): void
    {
        $event->setQueryPaginationParameter('test', 'val');
        parent::getPageItems($event);
    }
}
