<?php

namespace Jeprodev\Paginator\Tests\Mock;

use Jeprodev\Paginator\Event\PaginationEvent;
use Jeprodev\Paginator\Pagination\SlidingPagination;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MockPaginationSubscriber implements EventSubscriberInterface
{
    public function pagination(PaginationEvent $event)
    {
        $event->setPagination(new SlidingPagination());
        $event->stopPropagation();
    }

    public static function getSubscribedEvents()
    {
        return [
            'ajax_paginator.pagination' => ['pagination', 0]
        ];
    }
}