<?php

namespace Jeprodev\Paginator\Tests\Unit\Twig\Processor;

use Jeprodev\Paginator\Twig\Processor\PaginatorProcessor;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[CoversClass(PaginatorProcessor::class)]
class PaginatorProcessorTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    #[Test]
    public function shouldReturnUrlGenerationAndTranslation(): void
    {
        // Given
        /** @var UrlGeneratorInterface&MockObject $router */
        $router = $this->createMock(UrlGeneratorInterface::class);

        /** @var TranslatorInterface&MockObject $translator */
        $translator = $this->createMock(TranslatorInterface::class);

        $processor = new PaginatorProcessor($router, $translator);

        // Then
        $this->assertInstanceOf(UrlGeneratorInterface::class, $processor->getRouter());
        $this->assertInstanceOf(TranslatorInterface::class, $processor->getTranslator());
    }
}
