<?php

namespace Jeprodev\Paginator\Tests\Unit\Twig\Extension;

use Jeprodev\Paginator\Twig\Extension\PaginationExtension;
use Jeprodev\Paginator\Twig\Processor\PaginatorProcessor;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Twig\Extension\ExtensionInterface;

#[CoversClass(PaginationExtension::class)]
class PaginationExtensionTest extends TestCase
{
    protected function setUp(): void
    {
    }

    /**
     * @test
     */
    #[Test]
    public function shouldImplementTwigExtensionInterface(): void
    {
        // Given
        /** @var PaginatorProcessor&MockObject $processor */
        $processor = $this->createMock(PaginatorProcessor::class);

        $extension = new PaginationExtension($processor);

        // Then
        $this->assertInstanceOf(ExtensionInterface::class, $extension);
    }
}
