<?php

namespace Jeprodev\Paginator\Tests\Unit\DependencyInjection\Compiler;

use Jeprodev\Paginator\Definition\PaginatorAwareInterface;
use Jeprodev\Paginator\DependencyInjection\Compiler\PaginatorAwarePass;
use Jeprodev\Paginator\PaginatorInterface;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\Config\Definition\Exception\InvalidDefinitionException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

#[CoversClass(PaginatorAwarePass::class)]
class PaginatorAwarePassTest extends TestCase
{
    private $container;

    protected function setUp(): void
    {
        $this->container = new ContainerBuilder();
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfCompilerPassInterface(): void
    {
        // Given
        $compiler = new PaginatorAwarePass();

        // Then
        $this->assertInstanceOf(CompilerPassInterface::class, $compiler);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldCorrectPassProcess(): void
    {
        $this->container->register('ajax.paginator');
        $this->container->register('tag.one', PaginatorAwareInterface::class)
            ->addTag(PaginatorAwarePass::PAGINATOR_AWARE_TAG, ['paginator' => 'ajax.paginator']);

        (new PaginatorAwarePass())->process($this->container);

        $this->assertInstanceOf(ContainerBuilder::class, $this->container);
        $this->assertEquals(
            [['setPaginator', [new Reference('ajax.paginator')]]], 
            $this->container->getDefinition('tag.one')->getMethodCalls()
        );
    }

    /**
     * @test
     */
    #[Test]
    public function shouldExpectExceptionOnWrongInterface(): void
    {
        $this->container->register('ajax.paginator');
        $this->container->register('tag.one', 'stdClass')
            ->addTag(PaginatorAwarePass::PAGINATOR_AWARE_TAG, ['paginator' => 'ajax.paginator']);

        $this->assertInstanceOf(ContainerBuilder::class, $this->container);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Service "tag.one" must implement interface "Jeprodev\\Paginator\\Definition\\PaginatorAwareInterface".');

        (new PaginatorAwarePass())->process($this->container);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldExpectExceptionNoPaginator(): void
    {
        $this->container->register('tag.one', PaginatorAwareInterface::class)
            ->addTag(PaginatorAwarePass::PAGINATOR_AWARE_TAG, ['paginator' => 'INVALID']);

        $this->assertInstanceOf(ContainerBuilder::class, $this->container);
        $this->expectException(InvalidDefinitionException::class);
        $this->expectExceptionMessage('Paginator service "INVALID" for tag "ajax_paginator.injectable" on service "tag.one" could not be found.');

        (new PaginatorAwarePass())->process($this->container);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldProxyAndLazy(): void
    {
        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__ . '/../../../../config'));
        $loader->load('services.yaml');
        
        $this->container->register('ajax.paginator');

        $definition = $this->container->getDefinition('ajax_paginator');

        $this->assertInstanceOf(ContainerBuilder::class, $this->container);
        $this->assertSame([['interface' => PaginatorInterface::class]], $definition->getTag('proxy'));
        $this->assertTrue($definition->isLazy());
    }
}
