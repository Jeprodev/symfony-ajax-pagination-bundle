<?php

namespace Jeprodev\Paginator\Tests\Unit\DependencyInjection\Compiler;

use Jeprodev\Paginator\DependencyInjection\Compiler\PaginatorConfigurationPass;
use Jeprodev\Paginator\Tests\Unit\BaseTestCase;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class PaginatorConfigurationPassTest extends BaseTestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfCompilerPassInterface(): void
    {
        // Given
        $configurationPass = new PaginatorConfigurationPass();

        // Then
        $this->assertInstanceOf(CompilerPassInterface::class, $configurationPass);
    }

    /* *
     * @test
     * /
    #[Test]
    public function shouldNotProcessBundleConfiguration(): void
    {
        // Given
        $builder = new ContainerBuilder();
        $builder->register('ajax_paginator');
        $builder->register('tag.one', CompilerPassInterface::class);

        $compiler = new PaginatorConfigurationPass();

        // When
        $compiler->process($builder);

        // Then
        $this->assertEquals(1, 1);
    }

    /**
     * @test
     * /
    #[Test]
    public function shouldProcessBundleConfiguration(): void
    {
        // Given
        /** @var ContainerBuilder&MockObject $builder * /
        $builder = $this->createMock(ContainerBuilder::class);
        $builder->method('hasDefinition')->willReturn(true);
        $builder->method('hasAlias')->willReturn(true);

        $compiler = new PaginatorConfigurationPass();

        // When
        $compiler->process($builder);

        // Then
        $this->assertEquals(1, 1);
    }*/
}
