<?php

namespace Jeprodev\Paginator\Tests\Unit\DependencyInjection;

use Jeprodev\Paginator\DependencyInjection\Configuration;
use Jeprodev\Paginator\PaginatorInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;

#[CoversClass(Configuration::class)]
final class ConfigurationTest extends TestCase
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var Processor
     */
    private $processor;

    protected function setUp(): void
    {
        $this->configuration = new Configuration();
        $this->processor = new Processor();
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfConfigurationInterface(): void
    {
        // Then
        $this->assertInstanceOf(ConfigurationInterface::class, $this->configuration);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfTreeBuilder(): void
    {
        // When
        $configuration = $this->configuration->getConfigTreeBuilder();

        // Then
        $this->assertInstanceOf(TreeBuilder::class, $configuration);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldHaveAValidDefaultConfiguration(): void
    {
        // Given
        $treeBuilder = $this->configuration->getConfigTreeBuilder();

        $expected = [
            'default_options' => [
                'sort' => [
                    'field' => 'sort',
                    'direction' => 'direction'
                ],
                'filter' =>  [
                    'field' => 'field',
                    'value' => 'value'
                ],
                'pagination' => [
                    'name' => 'page',
                    'range' => 5,
                    'out_of_range' => PaginatorInterface::PAGE_OUT_OF_RANGE_IGNORE,
                    'limit' => 20
                ],
                'distinct' => true
            ],
            'template' => [
                'pagination' => '@Paginator/pagination.html.twig',
                'filtration' => '@Paginator/filter.html.twig',
                'sortable' => '@Paginator/sort.html.twig'
            ]
        ];
        
        // When
        $config = $this->processor->processConfiguration($this->configuration, []);

        // Then
        $this->assertInstanceOf(Configuration::class, $this->configuration);
        $this->assertInstanceOf(TreeBuilder::class, $treeBuilder);
        $this->assertEqualsCanonicalizing($expected, $config);
    }

    /**
     * @test
     * @return void
     */
    #[Test]
    public function shouldUpdateWithCustomConfiguration(): void
    {
        $treeBuilder = $this->configuration->getConfigTreeBuilder();

        $expected = [
            'default_options' => [
                'sort' => [
                    'field' => 'yup',
                    'direction' => 'sure'
                ],
                'filter' => [
                    'field' => 'hi',
                    'value' => 'there'
                ],
                'pagination' => [
                    'name' => 'bar',
                    'range' => 15,
                    'out_of_range' => PaginatorInterface::PAGE_OUT_OF_RANGE_FIX,
                    'limit' => 20
                ],
                'distinct' => false
            ],
            'template' => [
                'pagination' => '@Paginator/foo.html.twig',
                'filtration' => '@Paginatorbar.html.twig',
                'sortable' => '@Paginator/baz.html.twig'
            ]
        ];

        $config = $this->processor->processConfiguration($this->configuration, ["ajax_paginator" => $expected]);
        
        $this->assertInstanceOf(Configuration::class, $this->configuration);
        $this->assertInstanceOf(TreeBuilder::class, $treeBuilder);
        $this->assertEqualsCanonicalizing($expected, $config);
    }
}
