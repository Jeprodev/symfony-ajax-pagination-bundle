<?php

namespace Jeprodev\Paginator\Tests\Unit\DependencyInjection;

use Jeprodev\Paginator\DependencyInjection\Configuration;
use Jeprodev\Paginator\DependencyInjection\PaginatorExtension;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class PaginatorExtensionTest extends TestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldLoadPaginatorExtensionToContainer(): void
    {
        /** @var ContainerBuilder $builder */
        $builder = new ContainerBuilder();

        $processor = new Processor();

        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, []);
 
        // Given
        $extension = new PaginatorExtension();

        // When
        $extension->load([], $builder);

        // Then
        $this->assertTrue($builder->hasParameter('ajax_paginator.template.pagination'));
        $this->assertEquals($builder->getParameter('ajax_paginator.template.pagination'), $config['template']['pagination']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.template.filtration'));
        $this->assertEquals($builder->getParameter('ajax_paginator.template.filtration'), $config['template']['filtration']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.template.sortable'));
        $this->assertEquals($builder->getParameter('ajax_paginator.template.sortable'), $config['template']['sortable']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.filter.field'));
        $this->assertEquals($builder->getParameter('ajax_paginator.filter.field'), $config['default_options']['filter']['field']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.filter.value'));
        $this->assertEquals($builder->getParameter('ajax_paginator.filter.value'), $config['default_options']['filter']['value']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.sort.field'));
        $this->assertEquals($builder->getParameter('ajax_paginator.sort.field'), $config['default_options']['sort']['field']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.sort.direction'));
        $this->assertEquals($builder->getParameter('ajax_paginator.sort.direction'), $config['default_options']['sort']['direction']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.pagination.name'));
        $this->assertEquals($builder->getParameter('ajax_paginator.pagination.name'), $config['default_options']['pagination']['name']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.pagination.out_of_range'));
        $this->assertEquals($builder->getParameter('ajax_paginator.pagination.out_of_range'), $config['default_options']['pagination']['out_of_range']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.pagination.range'));
        $this->assertEquals($builder->getParameter('ajax_paginator.pagination.range'), $config['default_options']['pagination']['range']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.pagination.limit'));
        $this->assertEquals($builder->getParameter('ajax_paginator.pagination.limit'), $config['default_options']['pagination']['limit']);
        $this->assertTrue($builder->hasParameter('ajax_paginator.distinct'));
        $this->assertEquals($builder->getParameter('ajax_paginator.distinct'), $config['default_options']['distinct']);
    }
}
