<?php

namespace Jeprodev\Paginator\Tests\Unit;

use RuntimeException;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\EventManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\Mapping\ClassMetadataFactory;
use Doctrine\ORM\Mapping\DefaultQuoteStrategy;
use Doctrine\ORM\Mapping\DefaultNamingStrategy;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

abstract class BaseTestCaseOrm extends BaseTestCase
{
    /*protected ?EntityManager $entityManager;

    protected ?QueryAnalyzer $queryAnalyzer;

    public function setUp(): void
    {
    }

    protected function getMockSqliteEntityManager(?EventManager $eventManager = null): EntityManager
    {
        $connexion = ['driver' => 'pdo_sqlie', 'memory' => true];

        $config = $this->getMockAnnotedConfiguration();

        $entityManager = EntityManager::create($connexion, $config, $eventManager ?: $this->getEventManager());

        $schema = array_map(static function($class) use ($entityManager) {
            return $entityManager->getClassMetadata($class);
        }, (array)$this->getUserEntityFixtures());

        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->dropSchema([]);
        $schemaTool->createSchema($schema);

        return $this->entityManager = $entityManager;
    }

    protected function getMockCustomEntityManager(array $conn, ?EventManager $manager = null): EntityManager
    {
        $config = $this->getMockAnnotedConfiguration();
        $entityManager = EntityManager::create($conn, $config, $manager ?: $this->getEventManager());

        $schema = array_map(static function ($class) use ($entityManager) {
            return $entityManager->getClassMetadata($class);
        }, (array)$this->getUserEntityFixtures());

        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->dropSchema([]);
        $schemaTool->createSchema($schema);

        return $this->entityManager = $entityManager;
    }

    protected function getMockMappedEntityManager(?EventManager $manager = null): EntityManager
    {
        /**
         * @var Driver|MockObject
         * /
        $driver = $this->createMock(Driver::class);

        $driver->expects($this->once())->method('getDatabasePlatform')
            ->willReturn($this->createMock(MySQLPlatform::class));

        $connexion = $this->getMockBuilder(Connection::class)
            ->setConstructorArgs([[], $driver])->getMock();
        
        $connexion->expects($this->once())->method('getEventManager')->willReturn($manager ?: $this->getEventManager());

        $connexion = $this->getMockBuilder(Connection::class)
            ->setConstructorArgs([[], $driver])->getMock();

        $connexion->expects($this->once())->method('getEventManager')->willReturn($manager ?: $this->getEventManager());

        $config = $this->getMockAnnotedConfiguration();
        $this->entityManager = EntityManager::create($connexion, $config);
        return $this->entityManager;
    }

    protected function startQueryLog(): void
    {
        if (null === $this->entityManager) {
            throw new RuntimeException('Entity manager and database platform must be initialized');
        }

        $this->queryAnalyzer = new QueryAnalyzer($this->entityManager->getConnection()
            ->getDatabasePlatform());

        //$this->entityManager->getConfiguration()->method('getSQLLogger')->willReturn($this->queryAnalyzer);
    }

    protected function stopQueryLog(bool $dumpOnlySql = false, bool $writeToLog = false): void
    {
        if (null !== $this->queryAnalyzer) {
            ob_start();
            $this->queryAnalyzer->getOutput($dumpOnlySql);
            $output = ob_get_clean();
            
            if (!$writeToLog) {
                echo $output;
            } else {
                $fileName = __DIR__ . '/../../temp/query_debug_' . time() . '.log';

                if (($file = fopen($fileName, 'wb+')) === false) {
                    throw new RuntimeException("Can\'t write to the log file.");
                }
                fwrite($file, $output);
                fclose($file);
            }
        }
    }

    protected function getMetadataDriverImplementation(): AnnotationDriver
    {
        return new AnnotationDriver($_ENV['annotation_reader']);
    }

    abstract protected function getUserEntityFixtures(): array;

    private function getEventManager(): EventManager
    {
        return new EventManager();
    }

    /**
     * @return Configuration|\PHPUnit\Framework\MockObject\MockObject
     * /
    private function getMockAnnotedConfiguration()
    {
        /** 
         * @var Configuration&MockObject $mock 
         * /
        $mock = $this->createMock(Configuration::class);

        $mock->expects($this->once())->method('getProxyDirectory')
            ->willReturn(__DIR__ . '/../../temp');

        $mock->expects($this->once())->method('getProxyNamespace')
            ->willReturn('Proxy');

        $mock->expects($this->once())->method('getAutoGeneratedProxyClasses')
            ->willReturn(true);

        $mock->expects($this->once())->method('getClassMetadataFactoryName')
            ->willReturn(ClassMetadataFactory::class);

        $mappingDriver = $this->getMetadataDriverImplementation();

        $mock->expects($this->any())->method('getMetadataDriverImpl')
            ->willReturn($mappingDriver);

        $mock->expects($this->any())->method('getDefaultRepositoryClassName')
            ->willReturn(EntityRepository::class);

        $mock->expects($this->any())->method('getQuoteStrategy')
            ->willReturn(new DefaultQuoteStrategy());

        $mock->expects($this->any())->method('getNamingStrategy')
            ->willReturn(new DefaultNamingStrategy());

        $mock->expects($this->any())->method('getCustomHydrationMode')
            ->willReturn('Jeprodev\Paginator\Event\Subscriber\Paginate\Doctrine\ORM\Query\AsIsHydrator');

        $mock->expects($this->any())->method('getDefaultQueryHints')->willReturn([]);

        return $mock;
    }*/
}
