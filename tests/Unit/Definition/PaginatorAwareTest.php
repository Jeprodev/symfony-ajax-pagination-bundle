<?php

namespace Jeprodev\Paginator\Tests\Unit\Definition;

use Jeprodev\Paginator\Definition\PaginatorAware;
use Jeprodev\Paginator\Definition\PaginatorAwareInterface;
use Jeprodev\Paginator\Paginator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

#[CoversClass(PaginatorAware::class)]
class PaginatorAwareTest extends TestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfPaginatorAwareInterface(): void
    {
        // When
        $aware = new PaginatorAware();

        // Then
        $this->assertInstanceOf(PaginatorAwareInterface::class, $aware);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldImplementPaginatorGetterAndSetter(): void
    {
        // Given
        /** @var Paginator&MockObject $paginator */
        $paginator = $this->createMock(Paginator::class);

        $aware = new PaginatorAware();

        // When
        $aware->setPaginator($paginator);

        // Then
        $this->assertEquals($paginator, $aware->getPaginator());
    }
}
