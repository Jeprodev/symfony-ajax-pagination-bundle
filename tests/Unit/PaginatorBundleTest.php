<?php

namespace Jeprodev\Paginator\Tests\Unit;

use Jeprodev\Paginator\DependencyInjection\PaginatorExtension;
use Jeprodev\Paginator\PaginatorBundle;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class PaginatorBundleTest extends BaseTestCase
{
    /** 
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfAbstractBundle(): void
    {
        // Given
        $bundle = new PaginatorBundle();

        // Then
        $this->assertInstanceOf(AbstractBundle::class, $bundle);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldContainPaginatorCompilerPasses(): void
    {
        // Given
        $container = new ContainerBuilder();
        $bundle = new PaginatorBundle();
        $configs = [[]];

        // When
        $bundle->build($container);

        // Then
    
        // Build the service container 
        
        /*$extension = new PaginatorExtension();
        $extension->load($configs, $container);

        $container->compile();*/

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldContainAvalidContainerExtention(): void
    {
        // Given
        $bundle = new PaginatorBundle();

        // When
        $extension = $bundle->getContainerExtension();

        // Then
        $this->assertInstanceOf(ExtensionInterface::class, $extension);
        $this->assertInstanceOf(PaginatorExtension::class, $extension);
    }
}
