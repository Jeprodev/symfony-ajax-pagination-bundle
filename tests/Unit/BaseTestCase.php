<?php

namespace Jeprodev\Paginator\Tests\Unit;

use Jeprodev\Paginator\Event\Subscriber\Paginate\PaginationSubscriber;
use Jeprodev\Paginator\Event\Subscriber\Sortable\SortableSubscriber;
use Jeprodev\Paginator\Paginator;
use Jeprodev\Paginator\PaginatorInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcher;

class BaseTestCase extends TestCase
{
    /*protected function setUp(): void
    {
    }*/
    
    protected function getPaginatorInstance(?RequestStack $stack = null, ?EventDispatcher $dispacher = null): PaginatorInterface
    {
        if ($dispacher === null) {
            $dispacher = new EventDispatcher();
            $dispacher->addSubscriber(new PaginationSubscriber());
            $dispacher->addSubscriber(new SortableSubscriber());
        }
        
        return new Paginator($dispacher, $stack);
    }
/*
    protected function createRequestStack(array $params): RequestStack
    {
        $request = new Request($params);
        $stack = new RequestStack();
        $stack->push($request);

        return $stack;
    }*/
}