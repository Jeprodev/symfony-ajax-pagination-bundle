<?php

namespace Jeprodev\Paginator\Tests\Unit;

use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class QueryAnalyzer //implements SQLLogger
{
    /*protected AbstractPlatform $platform;

    private $queryStartTime = null;

    private $totalExecutionTime = 0;

    private array $queryExecutionTimes = [];

    private array $queries = [];

    public function __construct(AbstractPlatform $platform)
    {
        $this->platform = $platform;
    }

    public function getOutput($dumpSql = false): ?string
    {
        $output = '';

        if (!$dumpSql) {
            $output .= 'Plateform: ' . get_class($this->platform) . PHP_EOL;
            $output .= 'Executed queries: ' . count($this->queries) . ', total time : ' 
                . $this->totalExecutionTime . ' ms' . PHP_EOL;
        }

        foreach ($this->queries as $index => $query) {
            if (!$dumpSql) {
                $output .= 'Query(' . ($index + 1) . ') - ' . $this->queryExecutionTimes[$index] . ' ms' . PHP_EOL;
            }
            $output .= $query . ';' . PHP_EOL;
        }

        return $output . PHP_EOL;
    }

    public function startQuery($sql, ?array $params = null, ?array $types = null): void
    {
        $this->queryStartTime = microtime(true);
        $this->queries[] = $this->generateSql($sql, $params, $types);
    }

    public function stopQuery(): void
    {
        $ms = round(microtime(true) - $this->queryStartTime, 4) * 1000;
        $this->queryExecutionTimes[] = $ms;
        $this->totalExecutionTime += $ms;
    }

    public function cleanUp(): QueryAnalyzer
    {
        $this->queries = [];
        $this->queryExecutionTimes = [];
        $this->totalExecutionTime = 0;
        return $this;
    }

    private function generateSql(String $sql, array $params, array $types): string
    {
        if (!count($params)) {
            return $sql;
        }

        $converted = $this->getConvertedParams($params, $types);

        if (is_int(key($params))) {
            $index = key($params);

            $sql = preg_replace_callback('@\?@sm', static function($match) use ($index, $converted) {
                return implode(' ', $converted[$index++]);
            }, $sql);
        } else {
            foreach ($converted as $key => $value) {
                $sql = str_replace(':' . $key, $value, $sql);
            }
        }
        return $sql;
    }

    private function getConvertedParams(array $params, array $types): array
    {
        $result = [];
        foreach ($params as $position => $value) {
            if (isset($types[$position])) {
                $type = $types[$position];

                if (is_string($type)) {
                    $type = Type::getType($type);
                }

                if ($type instanceof Type) {
                    $value = $type->convertToDatabaseValue($value, $this->platform);
                }
            } else {
                if (is_object($value) && $value instanceof DateTime) {
                    $value = $value->format($this->platform->getDateFormatString());
                } elseif (!is_null($value)) {
                    $type = Type::getType(gettype($value));
                    $value = $type->convertToDatabaseValue($value, $this->platform);
                }
            }

            if (is_string($value)) {
                $value = "{$value}";
            } elseif (is_null($value)) {
                $value = "NULL";
            }

            $result[$position] = $value;
        }

        return $result;
    }*/
}