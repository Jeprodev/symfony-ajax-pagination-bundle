<?php

namespace Jeprodev\Paginator\Tests\Unit\Event;

use Jeprodev\Paginator\Event\PaginatorAfterEvent;
use Jeprodev\Paginator\Pagination\SlidingPagination;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\EventDispatcher\Event;

#[CoversClass(PaginatorAfterEvent::class)]
class PaginatorAfterEventTest extends TestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfEvent(): void
    {
        // Given
        $pagination = new SlidingPagination([]);

        // When
        $event = new PaginatorAfterEvent($pagination);

        // Then
        $this->assertInstanceOf(Event::class, $event);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldImplementPaginationGetter(): void
    {
        // Given
        $pagination = new SlidingPagination([]);

        $event = new PaginatorAfterEvent($pagination);

        // Then
        $this->assertEquals($pagination, $event->getPagination());
    }
}
