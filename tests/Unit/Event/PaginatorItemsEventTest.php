<?php

namespace Jeprodev\Paginator\Tests\Unit\Event;

use Jeprodev\Paginator\Event\PaginatorEvent;
use Jeprodev\Paginator\Event\PaginatorItemsEvent;
use Jeprodev\Paginator\Paginator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

#[CoversClass(PaginatorItemsEvent::class)]
class PaginatorItemsEventTest extends TestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfPaginatorEvent(): void
    {
        // Given
        $event = new PaginatorItemsEvent(20, 25);

        // Then
        $this->assertInstanceOf(PaginatorEvent::class, $event);
    }
    
    /**
     * @test
     */
    #[Test]
    public function shouldImplementCustomParameterSetterAndGetter(): void
    {
        // Given
        $event = new PaginatorItemsEvent(20, 20);

        // When
        $event->setQueryPaginationParameter(Paginator::DEFAULT_PAGE_ITEMS_LIMIT, 30);
        $parameters = $event->getQueryPaginationParameters();

        // Then
        $this->assertEquals(30, $parameters[Paginator::DEFAULT_PAGE_ITEMS_LIMIT]);

        // When
        $event->unsetQueryPaginationParameter(Paginator::DEFAULT_PAGE_ITEMS_LIMIT);
        $parameters = $event->getQueryPaginationParameters();

        // Then
        $this->assertFalse(isset($parameters[Paginator::DEFAULT_PAGE_ITEMS_LIMIT]));
    }
    
    /**
     * @test
     */
    #[Test]
    public function shouldImplementPageItemsCountGetterAndSetter(): void
    {
        // Given
        $event = new PaginatorItemsEvent(20, 20);

        // When
        $event->setPageItemsCount(41);

        // Then
        $this->assertEquals(41, $event->getPageItemsCount());
    }
    
    /**
     * @test
     */
    #[Test]
    public function shouldImplementPageLimitSetterAndGetter(): void
    {
        // Given
        $event = new PaginatorItemsEvent(15, 15);

        // When
        $event->setItemsPerPage(30);

        // Then
        $this->assertEquals(30, $event->getItemsPerPage());
    }

    /**
     * @test
     */
    #[Test]
    public function shouldImplementPaginatorOptionsGetterAndSetter(): void
    {
        // Given
        $event = new PaginatorItemsEvent(15, 15);

        $options = [
            Paginator::DEFAULT_PAGE_ITEMS_LIMIT => 25
        ];

        // When
        $event->setPaginatorOptions($options);

        // Then
        $this->assertEquals($options, $event->getPaginatorOptions());
    }
}
