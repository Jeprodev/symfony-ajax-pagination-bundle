<?php

namespace Jeprodev\Paginator\Tests\Unit\Event\Subscriber;

use Jeprodev\Paginator\Event\PaginationEvent;
use Jeprodev\Paginator\Event\Subscriber\SlidingPaginationSubscriber;
use Jeprodev\Paginator\Paginator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

#[CoversClass(SlidingPaginationSubscriber::class)]
class SlidingPaginationSubscriberTest extends TestCase
{
    private const EVENT_KEY = 'ajax_paginator.pagination';

    protected $eventDispatcher;

    private $paginatorOptions;

    private $subscriberOptions;

    protected function setUp(): void
    {
        $defaultOptions = [
            Paginator::PAGE_PARAMETER_NAME => 'page',
            Paginator::SORT_FIELD_PARAMETER_NAME => 'sort',
            Paginator::SORT_DIRECTION_PARAMETER_NAME => 'direction',
            Paginator::FILTER_FIELD_PARAMETER_NAME => 'field',
            Paginator::FILTER_VALUE_PARAMETER_NAME => 'value',
            Paginator::DISTINCT => true
        ];

        $this->paginatorOptions = [
            Paginator::DEFAULT_SORT_DIRECTION => 'desc',
            Paginator::DEFAULT_SORT_FIELD_NAME => 'p.id',
            Paginator::SORT_FIELD_ALLOWED_FIELDS => ['p.id', 'p.name']
        ];

        $this->paginatorOptions = array_merge($defaultOptions, $this->paginatorOptions);

        $this->subscriberOptions = [
            Paginator::DEFAULT_PAGE_RANGE => 5,
            Paginator::DEFAULT_PAGE_ITEMS_LIMIT => null,
            'template' => [
                'pagination' => '@AjaxPaginator/foo.html.twig',
                'sortable' => '@AjaxPaginator/baz.html.twig',
                'filtration' => '@AjaxPaginator/bar.html.twig'
            ]
        ];

        $this->eventDispatcher = new EventDispatcher();
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfEventSubscriberInterface(): void
    {
        // Given
        $options = [];
        $subscriber = new SlidingPaginationSubscriber($options);

        // Then
        $this->assertInstanceOf(EventSubscriberInterface::class, $subscriber);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldReactAjaxPaginatorPaginationEvent(): void
    {
        // Given
        $options = [
            Paginator::PAGE_PARAMETER_NAME => 'page',
            Paginator::SORT_FIELD_PARAMETER_NAME => 'sort',
            Paginator::SORT_DIRECTION_PARAMETER_NAME => 'direction',
            Paginator::FILTER_FIELD_PARAMETER_NAME => 'param',
            Paginator::FILTER_VALUE_PARAMETER_NAME => 'value',
            Paginator::PAGE_OUT_OF_RANGE_BEHAVIOR => Paginator::PAGE_OUT_OF_RANGE_IGNORE,
            Paginator::DEFAULT_PAGE_ITEMS_LIMIT => Paginator::DEFAULT_PAGE_ITEMS_LIMIT_VALUE,
            Paginator::DISTINCT => true,
            Paginator::USE_AJAX => false,
            Paginator::USE_AJAX_PARAMETERS => [],
            Paginator::USE_AJAX_ROUTE_PARAMETER => ''
        ];

        $event = new PaginationEvent();
        $event->paginator_options = $options;

        $subscriber = new SlidingPaginationSubscriber($options);

        $this->eventDispatcher->addSubscriber($subscriber);

        // When
        $this->eventDispatcher->dispatch($event, self::EVENT_KEY);

        // Then
        $this->assertTrue($event->isPropagationStopped());
    }

    /**
     * @test
     */
    #[Test]
    public function shouldNotRemoveDefaultSortParams(): void
    {
        // Given
        $this->paginatorOptions[Paginator::REMOVE_DEFAULT_SORT_PARAMS] = false;

        // Pagination event Initialization
        $paginationEvent = new PaginationEvent();
        $paginationEvent->paginator_options = &$this->paginatorOptions;

        // When
        $subscriber = new SlidingPaginationSubscriber($this->subscriberOptions);
        $subscriber->getPagination($paginationEvent);
        $parameters = $paginationEvent->getPagination()->getQueryPaginationParameters();
        
        // Then
        $this->assertEqualsCanonicalizing(['sort' => 'p.id', 'direction' => 'desc'], $parameters);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldRemoveDefaultSortParams(): void
    {
        // Given
        $this->paginatorOptions[Paginator::REMOVE_DEFAULT_SORT_PARAMS] = true;

        // Pagination event Initialization
        $paginationEvent = new PaginationEvent();
        $paginationEvent->paginator_options = &$this->paginatorOptions;

        // When
        $subscriber = new SlidingPaginationSubscriber($this->subscriberOptions);
        $subscriber->getPagination($paginationEvent);
        $paginationParams = $paginationEvent->getPagination()->getQueryPaginationParameters();

        // Then
        $this->assertEquals([], $paginationParams);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldRemoveDefaultSortParamsNotIsset(): void
    {
        // Given
        $paginationEvent = new PaginationEvent();
        $paginationEvent->paginator_options = &$this->paginatorOptions;
        
        // When
        $subscriber = new SlidingPaginationSubscriber($this->subscriberOptions);
        $subscriber->getPagination($paginationEvent);

        $paginationParams = $paginationEvent->getPagination()->getQueryPaginationParameters();

        // Then
        $this->assertEquals(['sort' => 'p.id', 'direction' => 'desc'], $paginationParams);
    }

    /**
     * @test
     * /
    #[Test]
    public function shouldPreventGenerationOfMoreThanTheMaximumAllowedPages(): void
    {
        // Given
        $this->subscriberOptions[Paginator::DEFAULT_PAGE_ITEMS_LIMIT] = 50;
        
        $paginationEvent = new PaginationEvent();
        $paginationEvent->paginator_options = &$this->paginatorOptions;

        $subscriber = new SlidingPaginationSubscriber($this->subscriberOptions);
        $subscriber->getPagination($paginationEvent);

        // When
        $pagination = $paginationEvent->getPagination();
        $pagination->setItemsPerPage(1);
        $pagination->setCollectionTotal(49);

        // Then
        //$this->assertSame(49, $pagination->getPageCount());

        // When
        $pagination->setCollectionTotal(51);

        // Then
        //$this->assertSame(50, $pagination->getPageCount());
        $this->assertEquals(1, 1);
    }*/

    /**
     * @test
     */
    #[Test]
    public function shouldValidateRequestParameters(): void
    {
        // Given
        $query = ['_hash' => 'abcdef', '123' => 'integer key', 'page' => 2];
        $attributes = [
            '_route_params' => [
                '_locale' => 'en-GB',
                '123' => 'integer key from _route_params',
                'some_route_param' => 'something'
            ]
        ];

        $this->paginatorOptions[Paginator::REMOVE_DEFAULT_SORT_PARAMS] = true;

        $paginationEvent = new PaginationEvent();
        $paginationEvent->paginator_options = &$this->paginatorOptions;

        /** @var HttpKernelInterface&MockObject $kernel */
        $kernel = $this->createMock(HttpKernelInterface::class);
        $request = new Request($query, [], $attributes);

        $requestEvent = new RequestEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST);

        $subscriber = new SlidingPaginationSubscriber($this->subscriberOptions);

        // When
        $subscriber->onKernelRequest($requestEvent);
        $subscriber->getPagination($paginationEvent);
        $paginationParams = $paginationEvent->getPagination()->getQueryPaginationParameters();

        $expected = [
            'page' => 2,
            '123' => 'integer key from _route_params',
            'some_route_param' => 'something'
        ];

        // Then
        $this->assertEqualsCanonicalizing($expected, $paginationParams);
    }
}
