<?php

namespace Jeprodev\Paginator\Tests\Unit;

use RuntimeException;
use Jeprodev\Paginator\Event\Subscriber\Paginate\PaginationSubscriber;
use Jeprodev\Paginator\Exception\InvalidPageLimitException;
use Jeprodev\Paginator\Exception\InvalidPageNumberException;
use Jeprodev\Paginator\Exception\PageNumberOutOfRangeException;
use Jeprodev\Paginator\Pagination\PaginationInterface;
use Jeprodev\Paginator\Paginator;
use Jeprodev\Paginator\PaginatorInterface;
use Jeprodev\Paginator\Tests\Mock\CustomParameterSubscriber;
use Jeprodev\Paginator\Tests\Mock\MockPaginationSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Jeprodev\Paginator\Tests\Unit\BaseTestCase;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(Paginator::class)]
class PaginatorTest extends BaseTestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldImplementPaginatorInterfaceWhenNoParametersSupplied(): void
    {
        // When
        $paginator = new Paginator();

        // Then
        $this->assertInstanceOf(PaginatorInterface::class, $paginator);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldImplementPaginatorInterface(): void
    {
        // When
        $paginator = $this->getPaginatorInstance();

        // Then
        $this->assertInstanceOf(PaginatorInterface::class, $paginator);
    }
    
    /**
     * @test
     */
    #[Test]
    public function shouldThrowExceptionWhenCollectionIsNull(): void
    {
        // Given
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber(new PaginationSubscriber());

        $paginator = $this->getPaginatorInstance(null, $dispatcher);

        // When
        $this->expectException(RuntimeException::class);

        $paginator->paginate(null, 1, 10);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldThrowExceptionOnInvalidPageArgs(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        // Then
        $this->expectException(InvalidPageNumberException::class);

        $paginator->paginate(['a', 'b'], -1, 0);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldThrowExceptionOnInvalidLimitArgs(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        // Then
        $this->expectException(InvalidPageLimitException::class);

        $paginator->paginate(['a', 'b'], 1, 0);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldThrowExceptionOnNegativeLimitArgs(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        // Then
        $this->expectException(InvalidPageLimitException::class);

        $paginator->paginate(['a', 'b'], 1, -4);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldProduceAnInstanceOfPaginationInterface(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();
        $items = range(1, 23);

        // When
        $pagination = $paginator->paginate($items, 1, 10);

        // Then
        $this->assertInstanceOf(PaginationInterface::class, $pagination);
    }

    /* *
     * @test
     * /
    #[Test]
    public function shouldUpdateItemsLimitWhenDifferentFromDefaultValue(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        $items = range(1, 53);

        $pagination = $paginator->paginate($items);

        $pagination->setPaginatorOptions([PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT => 8]);

        // When
        $paginationData = $pagination->getPaginationData();

        // Then
        $this->assertEquals(8, $paginationData['items_per_page']);
    }*/

    /* *
     * @test
     * /
    #[Test]
    public function shouldGiveCustomParametersToPagination(): void
    {
        // Given
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber(new CustomParameterSubscriber());
        $dispatcher->addSubscriber(new MockPaginationSubscriber());
        
        $paginator = new Paginator($dispatcher);
        $items = ['first', 'second'];

        // When
        // Then
        self::assertTrue(1 === 1);
    }

    /**
     * @test
     * /
    #[Test]
    public function shouldReorderSortParameterWhenArraySupplied(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();
        $items = range(1, 23);

        $options = [
            Paginator::SORT_FIELD_PARAMETER_NAME => [
                'name' => 'asc',
                'title' => 'asc'
            ]
        ];

        // When
        $pagination = $paginator->paginate($items, 1, 10, $options);

        // Then
        $this->assertEquals('name=asc+title=asc', $pagination->getSortField());
    }

    /**
     * @test
     * /
    #[Test]
    public function shouldThrowPageOutOfRangeExceptionWhenBehaveIsSetToThrowException(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();
        
        // Then
        $this->expectException(PageNumberOutOfRangeException::class);
        $paginator->paginate(
            range(1, 23),
            10,
            10,
            [
                Paginator::PAGE_OUT_OF_RANGE_BEHAVIOR => Paginator::PAGE_OUT_OF_RANGE_THROW_EXCEPTION
            ]
        );
    }

    /**
     * @test
     * /
    #[Test]
    public function should(): void
    {
        // Given
        // When
        // Then
        $this->assertEquals(1, 1);
    }

    /* *
     * @test
     * /
    #[Test]
    public function shouldHandleOutOfRangePageNumberAsArgument(): void
    {
        $paginator = $this->getPaginatorInstance();
        $items = range(1, 23);

        //fix options
        $options = [
            PaginatorInterface::PAGE_OUT_OF_RANGE => PaginatorInterface::PAGE_OUT_OF_RANGE_FIX
        ];

        $pagination = $paginator->paginate($items, 10, 10, $options);

        $paginationData = $pagination->getPaginationData();

        self::assertEquals(3, $paginationData['last_page_in_range']);
        self::assertEquals(3, $paginationData['current_page']);
        self::assertEquals(2, $paginationData['previous_page']);
        //self::assertEquals(5, $paginationData['current_item_number']);
        self::assertEquals(21, $paginationData['first_item_number']);
        //todo self::assertEquals(23, $paginationData['last_item_number']);

        // throw exception option
        $this->expectException(PageNumberOutOfRangeException::class);
        $paginator->paginate($items, 10, 10, [PaginatorInterface::PAGE_OUT_OF_RANGE => PaginatorInterface::PAGE_OUT_OF_RANGE_THROW_EXCEPTION]);
    }

    /* *
     * @test
     * /
    #[Test]
    public function shouldHandleOutOfRangePageNumberAsArgumentWithEmptyList(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();
        $items = [];

        // When
        $pagination = $paginator->paginate($items, 10, 10, [PaginatorInterface::PAGE_OUT_OF_RANGE => PaginatorInterface::PAGE_OUT_OF_RANGE_FIX]);
        $paginationData = $pagination->getPaginationData();

        self::assertEquals(1, $paginationData['last_page_in_range']);
        // since page out of range is set to 'fix' current page should be reset to values for one pa 
        self::assertEquals(1, $paginationData['current_page']);
        /*self::assertNull($paginationData['previous']);* /
        //self::assertEquals(0, $paginationData['current_item_number']);
        //self::assertEquals(1, $paginationData['first_item_number']);
        //self::assertEquals(0, $paginationData['last_item_number']);

        // throw exception option
        $this->expectException(PageNumberOutOfRangeException::class);
        $paginator->paginate($items, 10, 10, [PaginatorInterface::PAGE_OUT_OF_RANGE => PaginatorInterface::PAGE_OUT_OF_RANGE_THROW_EXCEPTION]);
    }

    /* *
     * @test
     * /
    #[Test]
    public function shouldHandleOutOfRangePageNumberAsDefaultOption(): void
    {
        $paginator = $this->getPaginatorInstance();
        $items = range(1, 23);
        $pagination = $paginator->paginate($items, 10, 10);
        $pagination->setPaginatorOptions([
            PaginatorInterface::PAGE_OUT_OF_RANGE => PaginatorInterface::PAGE_OUT_OF_RANGE_FIX
        ]);

        $data = $pagination->getPaginationData();

        $this->assertEquals(3, $data['last_page']);
        $this->assertEquals(3, $data['current_page']);
        $this->assertEquals(2, $data['previous_page']);
        $this->assertEquals(3, $data['current_item_number']);
        $this->assertEquals(21, $data['first_item_number']);
        $this->assertEquals(23, $data['last_item_number']);

        /*$this->expectException(PageNumberOutOfRangeException::class);
        $paginator->paginate($items, 10, 10, [
            PaginatorInterface::PAGE_OUT_OF_RANGE => PaginatorInterface::PAGE_OUT_OF_RANGE_THROW_EXCEPTION
        ]);* /
    }*/

    /**
     * @test
     */
    #[Test]
    public function shouldGetMaxPageWhenExceptionIsThrown(): void
    {
        $paginator = $this->getPaginatorInstance();
        $items = range(1, 23);

        try {
            $paginator->paginate($items, 10, 10, [PaginatorInterface::PAGE_OUT_OF_RANGE_BEHAVIOR => PaginatorInterface::PAGE_OUT_OF_RANGE_THROW_EXCEPTION]);
        } catch (PageNumberOutOfRangeException $e) {
            $this->assertEquals(3, $e->getMaxPageNumber());
        }
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAbleToTreatFirstPageAsValidWithEmptyList(): void
    {
        $paginator = $this->getPaginatorInstance();
        $items = [];

        $pagination = $paginator->paginate($items, 1, 10, [PaginatorInterface::PAGE_OUT_OF_RANGE_BEHAVIOR => PaginatorInterface::PAGE_OUT_OF_RANGE_FIX]);
        $data = $pagination->getPaginationData();

        self::assertEquals(1, $data['last_page_in_range']);
        self::assertEquals(1, $data['first_item_number']);
        self::assertEquals(0, $data['last_item_number']);
        self::assertEquals(1, $data['current_page']);
        self::assertFalse(isset($data['previous_page']));
        //self::assertEquals(0, $data['current_item_number']);

        // throw Exception option
        $this->expectException(PageNumberOutOfRangeException::class);
        $paginator->paginate($items, 1, 10, [PaginatorInterface::PAGE_OUT_OF_RANGE_BEHAVIOR => PaginatorInterface::PAGE_OUT_OF_RANGE_THROW_EXCEPTION]);
        
        self::assertEquals(0, $data['last_item_number']);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAbleToProducePagination(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        $items = range(1, 23);

        // When
        $pagination = $paginator->paginate($items, 1, 10);
        $pagination->renderer = static function ($data) {
            return 'custom';
        };

        // Then
        $data = $pagination->getPaginationData();

        $this->assertEquals(1, $data['first_page_in_range']);
        $this->assertEquals(3, $data['last_page_in_range']);
        $this->assertEquals(1, $data['current_page']);
        $this->assertEquals(10, $data['items_per_page']);
        $this->assertEquals(3, $data['page_count']);
        $this->assertEquals(23, $data['collection_total']);
        $this->assertEquals(2, $data['next_page']);
        $this->assertEquals(1, $data['first_item_number']);
        $this->assertEquals(10, $data['last_item_number']);
        $this->assertEquals([1, 2, 3], range(1, $data['page_range']));
        //$this->assertEquals('custom', (string)$pagination->re);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAbleToProducePaginationWithAjaxSettings(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        $items = range(1, 23);

        // When
        $pagination = $paginator->paginate($items, 3, 10, [
            Paginator::USE_AJAX => true,
            Paginator::USE_AJAX_ROUTE_PARAMETER => 'my-ajax-route'
        ]);

        $paginationData = $pagination->getPaginationData();

        // Then
        $this->assertTrue($paginationData['ajax_paginator']['pagination']['use_ajax']);
        $this->assertEquals('my-ajax-route', $paginationData['ajax_paginator']['pagination']['ajax_retrieve_route']);
    }
}