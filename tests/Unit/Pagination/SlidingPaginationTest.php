<?php

namespace Jeprodev\Paginator\Tests\Unit\Pagination;

use Doctrine\Common\Collections\ArrayCollection;
use Generator;
use Jeprodev\Paginator\Event\Subscriber\Paginate\ArraySubscriber;
use Jeprodev\Paginator\PaginatorInterface;
use Jeprodev\Paginator\Pagination\PaginationInterface;
use Jeprodev\Paginator\Pagination\SlidingPagination;
use Jeprodev\Paginator\Paginator;
use Jeprodev\Paginator\Tests\Mock\MockPaginationSubscriber;
use Jeprodev\Paginator\Tests\Unit\BaseTestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\EventDispatcher\EventDispatcher;

#[CoversClass(SlidingPagination::class)]
class SlidingPaginationTest extends BaseTestCase
{
    /**
     * @test
     */
    #[Test]
    public function shouldBeAnInstanceOfPaginationInterface(): void
    {
        // When
        $pagination = new SlidingPagination([]);

        // Then
        $this->assertInstanceOf(PaginationInterface::class, $pagination);
    }

    /**
     * @return Generator
     */
    public static function itemPerPageLimitProvider(): Generator
    {
        yield 'set to default value when null provided' => [null, Paginator::DEFAULT_PAGE_ITEMS_LIMIT_VALUE];
        yield 'set and retrieve the provided value' => [15, 15];
    }

    /**
     * @test
     * @dataProvider itemPerPageLimitProvider
     */
    #[Test]
    #[DataProvider('itemPerPageLimitProvider')]
    public function shouldImplementItemsPerPageSetterAndGetter(?int $value, int $expected): void
    {
        // Given
        $pagination = new SlidingPagination([]);

        // When
        $pagination->setItemsPerPage($value);

        // Then
        $this->assertEquals($expected, $pagination->getItemsPerPage());
    }

    /**
     * @test
     */
    #[Test]
    public function shouldImplementPaginationTotalItemsSetterAndGetter(): void
    {
        // Given
        $pagination = new SlidingPagination();

        // When
        $pagination->setCollectionTotal(25);

        // Then
        $this->assertEquals(25, $pagination->getCollectionTotal());
    }

    /**
     * @return Generator
     */
    public static function paginationCollectionProvider(): Generator
    {
        yield 'should return empty array if null provided' => [null, []];
        yield 'should return empty array if empty array provided' => [[], []];

        $collection = new ArrayCollection();
        yield 'should return empty collection' => [$collection, $collection];

        $collection = new ArrayCollection();
        $collection[] = 'a';
        $collection[] = 'm';
        yield 'should return collection with data' => [$collection, $collection];
    }

    /**
     * @test
     * @dataProvider paginationCollectionProvider
     */
    #[Test]
    #[DataProvider('paginationCollectionProvider')]
    public function shouldImplementPaginationCollectionSetterAndGetter(?iterable $items, iterable $expected): void
    {
        // Given
        $pagination = new SlidingPagination();

        // When
        $pagination->setCollection($items);

        // Then
        $this->assertSame($expected, $pagination->getCollection());
    }

    /**
     * @test
     * @dataProvider paginationCollectionProvider
     */
    #[Test]
    #[DataProvider('paginationCollectionProvider')]
    public function shouldImplementPageItemsSetterAndGetter(?iterable $items, iterable $expected): void
    {
        // Given
        $pagination = new SlidingPagination();

        // When
        $pagination->setPageItems($items);

        // Then
        $this->assertSame($expected, $pagination->getPageItems());
    }

    /**
     * @test
     */
    #[Test]
    public function shouldNotFallbackToPageInCaseIfExceedsItemLimit(): void
    {
        // Given
        $paginator = $this->getPaginatorInstance();

        // When
        $pagination = $paginator->paginate(range(1, 9), 2, 10);
        $items = $pagination->getPageItems();

        // Then
        $this->assertEmpty($items);
    }

    /**
     * @return Generator
     */
    public static function getPageLimitData(): Generator
    {
        yield [5, 120, 25, null];
        yield [1, 0, 25, null];
        yield [16, 400, 25, 3];
        yield [2, 26, 25, 3];
    }

    /**
     * @test
     * @dataProvider getPageLimitData
     */
    #[Test]
    #[DataProvider('getPageLimitData')]
    public function shouldGetPageCount(int $expected, int $totalItems, $itemsPerPage, ?int $limit): void
    {
        // Given
        $pagination = new SlidingPagination();

        // When
        $pagination->setCollectionTotal($totalItems);
        $pagination->setItemsPerPage($itemsPerPage);
        //$pagination->setItemsPerPage($limit);    
        
        // Then
        $this->assertSame($expected, $pagination->getPageCount());
    }

    /**
     * @return Generator
     */
    public static function getSortedData(): Generator
    {
        yield [true, 'title', 'asc', null];
        yield [true, 'title', 'asc', 'title'];
        yield [true, 'title+subtitle', 'asc', 'title+subtitle'];
        yield [true, 'title+subtitle', 'asc', ['title', 'subtitle']];
        yield [false, 'title', 'asc', 'subtitle'];
    }

    /**
     * @test
     * @dataProvider getSortedData
     */
    #[Test]
    #[DataProvider('getSortedData')]
    public function shouldSortItems(bool $expected, string $sort, string $direction, $key): void
    {
        // Given
        $pagination = new SlidingPagination();

        $pagination->setPaginatorOptions([
            Paginator::SORT_FIELD_PARAMETER_NAME => 'sort',
            Paginator::SORT_DIRECTION_PARAMETER_NAME => 'direction'
        ]);

        $pagination->setQueryPaginationParameter('sort', $sort);
        $pagination->setQueryPaginationParameter('direction', $direction);

        $this->assertSame($expected, $pagination->isSorted($key));
    }

    /**
     * @test
     */
    #[Test]
    public function shouldBeAbleToHandleNullLimit(): void
    {
        // Given
        $paginator = new Paginator();

        $items = range(1, 53);

        // When
        $pagination = $paginator->paginate($items, 2);

        $paginationData = $pagination->getPaginationData();

        // Then
        self::assertEquals(PaginatorInterface::DEFAULT_PAGE_ITEMS_LIMIT_VALUE, $paginationData['items_per_page']);
    }

    /**
     * @test
     */
    #[Test]
    public function shouldCustomizeParameterNames(): void
    {
        // Given
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber(new MockPaginationSubscriber());
        $dispatcher->addSubscriber(new ArraySubscriber());

        $paginator = new Paginator($dispatcher);

        $items = ['first', 'second'];

        // When
        $pagination = $paginator->paginate($items, 1, 10);

        // Then
        $this->assertEquals('page', $pagination->getPaginatorOption(PaginatorInterface::PAGE_PARAMETER_NAME));
        $this->assertEquals('sort', $pagination->getPaginatorOption(PaginatorInterface::SORT_FIELD_PARAMETER_NAME));
        $this->assertEquals('direction', $pagination->getPaginatorOption(PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME));
        $this->assertTrue($pagination->getPaginatorOption(PaginatorInterface::DISTINCT));
        $this->assertNull($pagination->getPaginatorOption(PaginatorInterface::SORT_FIELD_ALLOWED_FIELDS));

        // custom parameters
        $options = [
            PaginatorInterface::PAGE_PARAMETER_NAME => 'p',
            PaginatorInterface::SORT_FIELD_PARAMETER_NAME => 's',
            PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME => 'd',
            PaginatorInterface::DISTINCT => false,
            PaginatorInterface::SORT_FIELD_ALLOWED_FIELDS => ['a.f', 'a.d']
        ];

        // When
        $pagination = $paginator->paginate($items, 1, 10, $options);
        self::assertEquals('p', $pagination->getPaginatorOption(PaginatorInterface::PAGE_PARAMETER_NAME));
        self::assertEquals('s', $pagination->getPaginatorOption(PaginatorInterface::SORT_FIELD_PARAMETER_NAME));
        self::assertEquals('d', $pagination->getPaginatorOption(PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME));
        self::assertFalse($pagination->getPaginatorOption(PaginatorInterface::DISTINCT));
        self::assertEquals(['a.f', 'a.d'], $pagination->getPaginatorOption(PaginatorInterface::SORT_FIELD_ALLOWED_FIELDS));

        // Change default paginator options
        $pagination = $paginator->paginate($items, 1, 10);

        $pagination->setPaginatorOptions([
            PaginatorInterface::PAGE_PARAMETER_NAME => 'pg',
            PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME => 'dir',
            PaginatorInterface::SORT_FIELD_PARAMETER_NAME => 'srt'
        ]);

        // Then
        self::assertEquals('pg', $pagination->getPaginatorOption(PaginatorInterface::PAGE_PARAMETER_NAME));
        self::assertEquals('srt', $pagination->getPaginatorOption(PaginatorInterface::SORT_FIELD_PARAMETER_NAME));
        self::assertEquals('dir', $pagination->getPaginatorOption(PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME));
        self::assertTrue($pagination->getPaginatorOption(PaginatorInterface::DISTINCT));
    }
}
